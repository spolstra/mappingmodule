from setuptools import setup

setup(zip_safe=False, name='SesameMapping',
    version='1.2.1',
    description='Set of scripts and libraries to find an optimal app->arch mapping',
    author='Peter van Stralen, Simon Polstra, Stanley Jaddoe',
    author_email='p.vanstralen@uva.nl, s.polstra@uva.nl',
    packages=[
        'pyyml',
        'sesamemap',
        'pysesame',
        'sesamemap.misc',
        'sesamemap.optimize',
        'espam',
    ],
    package_data={
        'pyyml': ['YML.xsd', 'YML_Map.xsd', 'YML_FMap.xsd'],
    },
    install_requires=['lxml>=2.3', 'networkx>=1.7'],
)
