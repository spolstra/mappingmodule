import lxml.objectify
import lxml.etree
import os.path
import pyyml
import types

SCHEMA_FILE = os.path.join(os.path.dirname(pyyml.__file__), 'YML_FMap.xsd')
NAMESPACE = 'http://sesamesim.sourceforge.net/YML_FMap'
NSMAP = {None: NAMESPACE}


class YMLEntity(lxml.objectify.ObjectifiedElement):

    def get_source(self):
        return self.get('source')

    def get_dest(self):
        return self.get('dest')

    def set_source(self, source):
        self.set('source', str(source))

    def set_dest(self, dest):
        self.set('dest', str(dest))

    source = property(get_source, set_source)
    dest = property(get_dest, set_dest)


class YMLMultiApp(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}multiapplication' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def add_mapping(self, name, side):
        return lxml.etree.SubElement(self, "{%s}mapping" % NAMESPACE, side=str(side), name=str(name))

    def get_app(self, name):
        mapyml = self.xpath(
            './ns:mapping[@side="source" and @name="%s"]' % name,
                           namespaces={'ns': NAMESPACE})

        assert len(mapyml) == 1
        return mapyml[0]


class YMLMapping(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}mapping' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def add_mapping(self, name, side):
        return lxml.etree.SubElement(self, "{%s}mapping" % NAMESPACE, side=str(side), name=str(name))

    def add_group(self, source, dest):
        e = lxml.etree.SubElement(
            self, "{%s}faultgroup" %
            NAMESPACE, side='source', name=str(source))
        return lxml.etree.SubElement(e, "{%s}faultgroup" % NAMESPACE, side='dest', name=str(dest))

    def get_groups(self):
        return self.xpath("ns:faultgroup", namespaces={'ns': NAMESPACE})

    def get_inner_mapping(self):
        '''Get inner mapping environment for adding maps'''
        # get_parent returns None for root node
        parent = self.getparent()
        if parent is not None and parent == '{%s}mapping' % (NAMESPACE):
            return self

        child = self.findall(".//{%s}mapping" % (NAMESPACE))
        assert len(child) == 1 and self.get_side() != child[0].get_side()
        return child[0]

    def add_instruction(self, source, dest):
        return lxml.etree.SubElement(self, "{%s}instruction" % NAMESPACE, source=str(source), dest=str(dest))

    def add_map(self, source, dest, dtmpl, args=None):
        e = lxml.etree.SubElement(self, "{%s}map" % NAMESPACE)
        e.set('source', source)
        e.set('dest', dest)
        e.set('dtmpl', dtmpl)
        if isinstance(args, str):
            e.set('arg', args)
        elif isinstance(args, list):
            e.set('arg', ','.join(args))
        return e

    def get_side(self):
        return self.get('side')

    def get_name(self):
        return self.get('name')

    def set_side(self, side):
        self.set('side', str(side))

    def set_name(self, name):
        self.set('name', str(name))

    # SP: get mapping of a component
    # This will not work with nested mapping of the same name.
    def get_map(self, comp):
        for e in self.findall(".//{%s}map[@source='%s']" %
                              (NAMESPACE, comp))[0].iter():
            return e
        return None

    side = property(get_side, set_side)
    name = property(get_name, get_side)


class YMLPort(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}port' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)


class YMLInstruction(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}instruction' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)


class YMLMap(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}map' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def add_port(self, source, dest):
        return lxml.etree.SubElement(self, "{%s}port" % NAMESPACE, dest=str(dest), source=str(source))

    def get_dtmpl(self):
        return self.get('dtmpl')

    def set_dtmpl(self, dtmpl):
        self.set('dtmpl', str(dtmpl))

    def get_darg(self):
        return self.get('arg')

    def set_darg(self, val):
        self.set('arg', str(val))

    def get_source(self):
        return self.get('source')

    def get_dest(self):
        return self.get('dest')

    def set_dest(self, val):
        self.set('dest', val)

    def toXML(self):
        print "BARFOO"

    dtmpl = property(get_dtmpl, set_dtmpl)
    darg = property(get_darg, set_darg)
    dest = property(get_dest, set_dest)


class YMLControl(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}control' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_main_tmpl(self):
        return self.get('main_tmpl')

    def set_main_tmpl(self, dtmpl):
        self.set('main_tmpl', str(dtmpl))

    def get_repl_tmpl(self):
        return self.get('repl_tmpl')

    def set_repl_tmpl(self, dtmpl):
        self.set('repl_tmpl', str(dtmpl))

    def get_dest(self):
        return self.get('dest')

    def set_dest(self, val):
        if hasattr(val, '__iter__'):
            assert all(' ' not in v for v in val)
            self.set('dest', ' '.join(val))
        else:
            assert ' ' not in val
            self.set('dest', val)

    def get_args(self):
        return self.get('arg')

    def toXML(self):
        print "BARFOO"

    dest = property(get_dest, set_dest)


class YMLElement(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}element' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_main_tmpl(self):
        return self.get('main_tmpl')

    def set_main_tmpl(self, dtmpl):
        self.set('main_tmpl', str(dtmpl))

    def get_repl_tmpl(self):
        return self.get('repl_tmpl')

    def set_repl_tmpl(self, dtmpl):
        self.set('repl_tmpl', str(dtmpl))

    def get_source(self):
        return self.get('source')

    def get_dest(self):
        return self.get('dest')

    def set_dest(self, val):
        if hasattr(val, '__iter__'):
            assert all(' ' not in v for v in val)
            self.set('dest', ' '.join(val))
        else:
            assert ' ' not in val
            self.set('dest', val)

    def get_args(self):
        return self.get('arg')

    def toXML(self):
        print "BARFOO"

    dest = property(get_dest, set_dest)


class YMLBuffer(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}buffer' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_dtmpl(self):
        return self.get('dtmpl')

    def set_dtmpl(self, dtmpl):
        self.set('dtmpl', str(dtmpl))

    def get_source(self):
        return self.get('source')

    def get_dest(self):
        return self.get('dest')

    def set_dest(self, val):
        if hasattr(val, '__iter__'):
            assert all(' ' not in v for v in val)
            self.set('dest', ' '.join(val))
        else:
            assert ' ' not in val
            self.set('dest', val)

    def toXML(self):
        print "BARFOO"

    dest = property(get_dest, set_dest)


class YMLGroup(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}faultgroup' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def set_dest(self, dests):
        assert all(' ' not in v for v in dests)
        dest = ' '.join(dests)
        for child in self.iterchildren():
            child.set('dest', dest)

    def add_control(self, main_tmpl, repl_tmpl, args):
        e = lxml.etree.SubElement(self, "{%s}control" % NAMESPACE)
        e.set('main_tmpl', main_tmpl)
        e.set('repl_tmpl', repl_tmpl)
        e.set('arg', ','.join(args))
        return e

    def add_element(self, source, main_tmpl, repl_tmpl, nevent):
        e = lxml.etree.SubElement(self, "{%s}element" % NAMESPACE)
        e.set('source', source)
        e.set('main_tmpl', main_tmpl)
        e.set('repl_tmpl', repl_tmpl)
        e.set('arg', str(nevent))
        return e

    def add_buffer(self, source, tmpl):
        e = lxml.etree.SubElement(self, "{%s}buffer" % NAMESPACE)
        e.set('source', source)
        e.set('dtmpl', tmpl)
        return e

    def get_dest_group(self):
        '''Get dest group environment for tmpl'''
        if self.get('side') == 'dest':
            return self

        dgroup = self.getparent()
        if dgroup != '{%s}faultgroup' % (NAMESPACE):
            child = self.findall(".//{%s}faultgroup" % (NAMESPACE))
            assert len(child) == 1
            dgroup = child[0]

        assert dgroup.get('side') == 'dest'
        return dgroup

    def get_inner_group(self):
        '''Get inner group environment for adding maps'''
        parent = self.getparent()
        if parent == '{%s}faultgroup' % (NAMESPACE):
            return self

        child = self.findall(".//{%s}faultgroup" % (NAMESPACE))
        assert len(child) == 1 and self.get_side() != child[0].get_side()
        return child[0]

    def get_nodes(self):
        maps = self.findall(".//{%s}map" % (NAMESPACE))
        return filter(lambda x: '->' not in x.get_source(), maps)

    def get_side(self):
        return self.get('side')

    def get_name(self):
        return self.get('name')

    def get_tmpl(self):
        return self.get('ptmpl')

    def set_side(self, side):
        self.set('side', str(side))

    def set_name(self, name):
        self.set('name', str(name))

    # SP: get mapping of a component
    # This will not work with nested mapping of the same name.
    def get_map(self):
        map = self.get_control().get('dest')
        for e in self.findall(".//{%s}element|.//{%s}buffer" % (NAMESPACE, NAMESPACE)):
            assert e.get('dest') == map
        return map.split()

    def get_control(self):
        return self.findall('.//{%s}control' % (NAMESPACE))[0]

    def get_elements(self):
        return self.findall('.//{%s}element' % (NAMESPACE))

    def get_buffers(self):
        return self.findall('.//{%s}buffer' % (NAMESPACE))

    side = property(get_side, set_side)
    name = property(get_name, get_side)


def parse(filename, validate=True):
    parser = lxml.etree.XMLParser(ns_clean=True, remove_blank_text=True)
    cls_lookup = lxml.etree.ElementNamespaceClassLookup()
    parser.set_element_class_lookup(cls_lookup)

    namespace = cls_lookup.get_namespace(NAMESPACE)
    namespace[None] = YMLEntity
    namespace['multiapplication'] = YMLMultiApp
    namespace['mapping'] = YMLMapping
    namespace['faultgroup'] = YMLGroup
    namespace['map'] = YMLMap
    namespace['instruction'] = YMLInstruction
    namespace['port'] = YMLPort
    namespace['buffer'] = YMLBuffer
    namespace['control'] = YMLControl
    namespace['element'] = YMLElement

    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()

    if validate:
        xmlschema_doc = lxml.etree.parse(file(SCHEMA_FILE))
        xmlschema = lxml.etree.XMLSchema(xmlschema_doc)
        xmlschema.assertValid(tree)

    return tree.getroot()


def new_fmapping(source, dest, multiapp):
    '''Make new mapping from a specific source to a specific architecture'''
    parser = lxml.etree.XMLParser(ns_clean=True)
    cls_lookup = lxml.etree.ElementNamespaceClassLookup()
    parser.set_element_class_lookup(cls_lookup)

    namespace = cls_lookup.get_namespace(NAMESPACE)
    namespace[None] = YMLEntity
    namespace['multiapplication'] = YMLMultiApp
    namespace['mapping'] = YMLMapping
    namespace['faultgroup'] = YMLGroup
    namespace['map'] = YMLMap
    namespace['instruction'] = YMLInstruction
    namespace['port'] = YMLPort
    namespace['buffer'] = YMLBuffer
    namespace['control'] = YMLControl
    namespace['element'] = YMLElement

    if multiapp:
        root = parser.makeelement("{%s}multiapplication" % NAMESPACE)
        for app in source:
            approot = root.add_mapping(app, 'source')
            approot.add_mapping(dest, 'dest')
    else:
        root = parser.makeelement("{%s}mapping" % NAMESPACE)
        root.set('side', 'source')
        root.set('name', source[0])

        root.add_mapping(dest, 'dest')
    return root


def store_fmapping(filename, mapping):
    tree = mapping.getroottree()
    tree.write(
        filename,
        pretty_print=True,
     xml_declaration=True,
     encoding='UTF-8')


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <ymlfmap-file>" % sys.argv[0]
        sys.exit(2)
    root = parse(sys.argv[1])

    print 'PARSED FAULT TOLERANT MAPPING:'
    print lxml.objectify.dump(root)
    print ''

    newmap = new_fmapping('application', 'architecture', False)
    group = newmap.add_group('group1', 'fpga0')
    group.add_control('vgactive', 'vlactive', ['0', '2'])
    group.add_element('A', 'vstate', 'vproc', 99)
    group.add_element('B', 'vstate', 'vproc', 99)
    group.add_buffer('A.out0->B.in0', 'vself')
    group.set_dest(['p0', 'p1'])

    group = newmap.add_group('group2', 'fpga0')
    group.add_control('vgactive', 'vlactive', ['1', '3'])
    group.add_element('C', 'vstate', 'vproc', 99)
    group.set_dest(['p2'])

    newmap.add_map('OWP0', 'IO', 'vio', ['0', '0', '0'])
    newmap.add_map('OWP1', 'IO', 'vio', '0,0,0')
    newmap.add_map('OWP0.out0->A.in0', 'memory', 'vmembus')
    newmap.add_map('B.out0->C.in0', 'memory', 'vmembus')
    newmap.add_map('C.out0->OWP1.in0', 'memory', 'vmembus')

    print 'SINGLE APPLICATION MAPPING:'
    print lxml.objectify.dump(newmap)
    print ''

    newmap = new_fmapping(['app0', 'app1', 'app2'], 'architecture', True)
    app = newmap.get_app('app0')
    app.add_group('group0', 'fpga')
    app = newmap.get_app('app1')
    app.add_group('group1', 'fpga')
    app = newmap.get_app('app2')
    app.add_group('group2', 'fpga')

    print 'PARTIAL MULTIPLE APPLICATION MAPPING:'
    print lxml.objectify.dump(newmap)
    print ''
