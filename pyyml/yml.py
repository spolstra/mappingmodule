import lxml.objectify
import lxml.etree
import lxml.pyclasslookup
import networkx as NX
import os.path
import logging
import pyyml

SCHEMA_FILE = os.path.join(os.path.dirname(pyyml.__file__), 'YML.xsd')

NAMESPACE = 'http://sesamesim.sourceforge.net/YML'
YML = "{%s}" % NAMESPACE
NSMAP = {None: NAMESPACE}

parser = None

# setup logger
logger = logging.getLogger(__name__.split('.')[-1])
#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class YMLError(Exception):
    def __init__(self, msg, line=None):
        self.msg = msg
        self.line = line

    def __str__(self):
        if self.line:
            return "Line %d: " % self.line + self.msg
        else:
            return self.msg

class YMLEntity(lxml.objectify.ObjectifiedElement):

    def print_path(self):
        ancestors = [self] + [a for a in self.iterancestors()]
        ancestors.reverse()
        # doesn't work when a YMLLink is one of the ancestors
        return '.'.join(a.name for a in ancestors)

    def docs(self):
        return self.xpath('ns:doc', namespaces={'ns': NAMESPACE})

    def properties(self):
        return self.xpath('ns:property', namespaces={'ns': NAMESPACE})

    def get_property(self, name):
        return self.xpath("ns:property[@name='%s']" % name,
                          namespaces={'ns': NAMESPACE})

    def set_property(self, name, value):
        return self.add_property(name, value, replace=True)

    def add_property(self, name, value, replace=False):
        properties = self.get_property(name)
        if replace and properties:
            new_property = properties[0]
        else:
            new_property = lxml.etree.SubElement(self,
                                                 "{%s}property" % NAMESPACE, name=str(name))
        # TODO: prefered way: new_property.value = value
        new_property.set('value', str(value))
        return new_property

    def remove_all_properties(self, name):
        for p in (self.get_property(name)):
            self.remove(p)

    def get_prop_val(self, name):
        p = self.get_property(name)
        if p:
            return p[0].get_value()
        else:
            raise YMLError("Property '%(name)s' not found in"
                           " component '%(self)s'" % locals())

    def get_prop_val_safe(self, name):
        p = self.get_property(name)
        if p:
            return p[0].get_value()
        else:
            return None

    def getparentnetwork(self):
        p = self
        while p is not None:
            if p.tag == '{%s}network' % NAMESPACE:
                return p
            p = p.getparent()
        return None

    # Return node or network
    def get_node_or_network(self, name):
        node_or_net = self.xpath(
            "ns:node[@name='%s']|ns:network[@name='%s']" %
                      (name, name), namespaces={'ns': NAMESPACE})
        if len(node_or_net) != 1:
            raise YMLError("Cannot find node/network: \"%s\"" % name ,
                           self.sourceline)
        return node_or_net


class YMLNode(YMLEntity):

    def get_port(self, name):
        p = self.xpath("ns:port[@name='%s']" %
                       name, namespaces={'ns': NAMESPACE})
        if len(p) != 1:
            raise YMLError("Cannot find port: \"%s\"" % name)
        return p

    def check_port(self, name):
        return len(self.xpath("ns:port[@name='%s']" %
                              name, namespaces={'ns': NAMESPACE})) == 1

    def scripts(self):
        return self.xpath('ns:script', namespaces={'ns': NAMESPACE})

    def ports(self):
        return self.xpath('ns:port', namespaces={'ns': NAMESPACE})

    # SP: Returns list of outports
    def outports(self):
        return self.xpath("ns:port[@dir='out']",
                          namespaces={'ns': NAMESPACE})

    def get_name(self):
        return self.get('name')

    def get_class(self):
        return self.get('class')

    def add_port(self, name, pdir='out'):
        # Check if there already is a port with that name
        # Don't want to raise an exception here.
        if self.check_port(name):
            p = self.get_port(name)
        else:
            p = lxml.etree.SubElement(self, "{%s}port" % NAMESPACE,
                                      name=name, dir=pdir)
        return p

    name = property(get_name)
    tclass = property(get_class)

    def __str__(self):
        return "node %s" % (self.name)


class YMLNetwork(YMLNode):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.set('class', 'net')
        self.tag = '{%s}network' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_node(self, name):
        n = self.xpath("ns:node[@name='%s']" % name,
                       namespaces={'ns': NAMESPACE})
        if len(n) != 1:
            raise YMLError("Cannot find node: \"%s\"" % name)
        return n

    def get_link(self, innode, inport, outnode, outport):
        return self.xpath("ns:link[@innode='%s' and @inport='%s' and"
                          " @outnode='%s' and @outport='%s']" %
                          (innode, inport, outnode, outport),
                          namespaces={'ns': NAMESPACE})

    def get_network(self, name):
        return self.xpath("ns:network[@name='%s']" %
                          name, namespaces={'ns': NAMESPACE})

    def nodes(self):
        return self.xpath('ns:node', namespaces={'ns': NAMESPACE})

    def networks(self):
        return self.xpath('ns:network', namespaces={'ns': NAMESPACE})

    def links(self):
        return self.xpath('ns:link', namespaces={'ns': NAMESPACE})

    def links_from(self, name):
        """Find links originating from name."""
        return self.xpath("ns:link[@innode='%s']" % name,
                          namespaces={'ns': NAMESPACE})

    def add_link(self, innode, inport, outnode, outport):
        oldlink = self.get_link(innode, inport, outnode, outport)
        if len(oldlink) == 0:
            p = lxml.etree.SubElement(self, "{%s}link" % NAMESPACE,
                                      innode=innode, inport=inport, outnode=outnode, outport=outport)
            return p
        return oldlink[0]

    def __str__(self):
        return "network %s" % (self.name)


class YMLMultiApp(YMLNode):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}multiapplication' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_node(self, name):
        return self.xpath("ns:node[@name='%s']" %
                          name, namespaces={'ns': NAMESPACE})

    def get_network(self, name):
        return self.xpath("ns:network[@name='%s']" %
                          name, namespaces={'ns': NAMESPACE})

    def apps(self):
        return self.xpath('ns:network', namespaces={'ns': NAMESPACE})

    def nodes(self):
        allnodes = []
        for app in self.apps():
            allnodes.extend([n for n in app.nodes()])
        return allnodes

    def __str__(self):
        return "multiapplication"


class YMLProperty(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self)
        self.tag = '{%s}property' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_name(self):
        return self.get('name')

    def get_value(self):
        return self.get('value')

    def set_value(self, value):
        # TODO: this setter doesn't work for some reason
        # use the set() function as a temporary workaround
        self.set('value', str(value))

    name = property(get_name)
    value = property(get_value, set_value)

    def __str__(self):
        return "property %s: %s" % (self.name, self.value)


class YMLPort(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self)
        self.tag = '{%s}port' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_datadir(self):
        dp = [p.value for p in self.get_property('datadir')]
        if dp:
            return dp[0]
        else:
            return 'both'

    def get_name(self):
        return self.get('name')

    def on_network_boundary(self):
        return self.getparent() == self.getparentnetwork()

    def __str__(self):
        portname = "port '%s'" % self.get_name()
        if self.datadir == 'in':
            return "%s (write only)" % portname
        elif self.datadir == 'out':
            return "%s (read only)" % portname
        else:
            return portname

    datadir = property(get_datadir)
    name = property(get_name)


class YMLLink(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}link' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_nodes(self):
        parent = self.getparent()
        innode = self.get('innode')
        outnode = self.get('outnode')
        logger.debug("parent %s, innode %s, outnode %s" % (parent, innode,
            outnode))
        try:
            innode = (parent if innode == 'this' else (
                parent.get_node_or_network(innode))[0])
            outnode = (parent if outnode == 'this' else (
                parent.get_node_or_network(outnode))[0])
        except YMLError as e:
            e.line = self.sourceline
            raise e

        return innode, outnode

    def get_ports(self):
        inport = self.get('inport')
        outport = self.get('outport')
        innode, outnode = self.get_nodes()
        try:
            inp = innode.get_port(inport)[0]
            outp = outnode.get_port(outport)[0]
        except YMLError as e:
            innode, outnode = self.get_nodes()
            print "Error: Link between \"%s\" and \"%s\" contains"\
                " unknown ports:" % (innode.name, outnode.name)
            print e
            raise

        return inp, outp

    def linkID(self):
        return self.__str__()

    nodes = property(get_nodes)
    ports = property(get_ports)

    def __cmp__(self, o):
        return cmp(self.__str__(), o.__str__())

    def __str__(self):
        innode, outnode = self.get_nodes()
        inport, outport = self.get_ports()
        return "%s.%s->%s.%s" % (innode.name, inport.name,
                                 outnode.name, outport.name)


# FIXME: YMLMap stuff should be removed from yml.
# The problem is that we have mapping element in virtual and arch yml.
# To deal with these we need a lxml objectify mapping. But it is
# easy to confuse these with the ymlmap stuff.
class YMLMap(YMLEntity):

    def __init__(self, source=None, dest=None):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}map' % NAMESPACE

        if source is not None:
            self.set("source", source)

        if dest is not None:
            self.set("dest", dest)

    def get_source(self):
        return self.get("source")

    def get_dest(self):
        return self.get("dest")

    source = property(get_source)
    dest = property(get_dest)


class YMLMapping(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}mapping' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def get_side(self):
        return self.get("side")

    def get_name(self):
        return self.get("name")

    side = property(get_side)
    name = property(get_name)


class DefaultYMLLookup(lxml.pyclasslookup.PythonElementClassLookup):

    def properties(self, element, name):
        return (p for p in element.getchildren()
                if p.tag == '{%s}property' % NAMESPACE and
                p.get('name') == name)

    def ancestors(self, element):
        ancestorslist = []
        while element:
            parent = element.getparent()
            ancestorslist.append(parent)
            element = parent
        ancestorslist.reverse()
        return ancestorslist[1:]

    def lookup(self, document, element):
        if element.tag == '{%s}multiapplication' % NAMESPACE:
            return YMLMultiApp
        if element.tag == '{%s}network' % NAMESPACE:
            return YMLNetwork
        elif element.tag == '{%s}node' % NAMESPACE:
            return YMLNode
        elif element.tag == '{%s}property' % NAMESPACE:
            return YMLProperty
        elif element.tag == '{%s}link' % NAMESPACE:
            return YMLLink
        elif element.tag == '{%s}port' % NAMESPACE:
            return YMLPort
        elif element.tag == '{%s}doc' % NAMESPACE:
            return lxml.objectify.StringElement
        elif element.tag == '{%s}script' % NAMESPACE:
            return lxml.objectify.StringElement
        elif element.tag == '{%s}mapping' % NAMESPACE:
            return YMLMapping
        elif element.tag == '{%s}map' % NAMESPACE:
            return YMLMap
        else:
            return YMLEntity


def parse(filename, class_lookup=DefaultYMLLookup, validate=True):
    global parser
    parser = lxml.etree.XMLParser(remove_blank_text=True, remove_comments=True)
    parser.setElementClassLookup(class_lookup())

    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()

    if validate:
        xmlschema_doc = lxml.etree.parse(file(SCHEMA_FILE))
        xmlschema = lxml.etree.XMLSchema(xmlschema_doc)
        xmlschema.assertValid(tree)

    return tree.getroot()


def fromstring(text, class_lookup=DefaultYMLLookup, validate=True):
    global parser
    parser = lxml.etree.XMLParser(remove_blank_text=True, remove_comments=True)
    parser.setElementClassLookup(class_lookup())

    tree = lxml.etree.fromstring(text, parser)

    if validate:
        xmlschema_doc = lxml.etree.parse(file(SCHEMA_FILE))
        xmlschema = lxml.etree.XMLSchema(xmlschema_doc)
        xmlschema.assertValid(tree)

    return tree


class YMLGraph(NX.DiGraph):

    def __init__(self, yml_root, data=None, name=None, **kwds):
        self.yml = yml_root
        NX.DiGraph.__init__(self, data=None, name=name, selfloops=False,
                            multiedges=False, **kwds)
        if data is not None:
            data = data.copy()
            self.adj = data.adj
            self.pred = data.pred
            self.succ = data.succ

    def copy(self):
        """Return a (shallow) copy of the YMLGraph.

        Identical to dict.copy() of adjacency dicts pred and succ,
        with name copied as well.

        """
        H = self.__class__(self.yml)
        H.name = self.name
        H.adj = self.adj.copy()
        H.succ = H.adj  # fix pointer again
        for v in H.adj:
            H.pred[v] = self.pred[v].copy()
            H.succ[v] = self.succ[v].copy()
        return H

    def __reverse_datadir(self, dd):
        if dd == 'in':
            return 'out'
        elif dd == 'out':
            return 'in'
        elif dd == 'none':
            return 'both'
        else:
            return 'both'

    def edge_allowed(self, inport, outport):
        if inport.datadir == 'none' or outport.datadir == 'none':
            return False
        inparent, outparent = inport.getparent(), outport.getparent()
        innetwork, outnetwork = (inport.getparentnetwork(),
                                 outport.getparentnetwork())
        inport_datadir = inport.datadir
        outport_datadir = outport.datadir
        if innetwork == outnetwork:
            # Both ports belong to the same network, check which port is on the
            # boundary of this network. If a port is on the boundary, reverse
            # the datadir value.
            if inport.on_network_boundary():
                inport_datadir = self.__reverse_datadir(inport_datadir)
            if outport.on_network_boundary():
                outport_datadir = self.__reverse_datadir(outport_datadir)
            if inparent == outparent and not(inport.on_network_boundary()
                                             or outport.on_network_boundary()):
                # Both ports belong to the same node. It is assumed that within
                # a node, communication between any port is possible.
                return True

        return inport_datadir != 'in' and outport_datadir != 'out'

    def add_edge(self, xxx_todo_changeme, yml_link=None):
        # connect the ports in the allowed directions
        (inport, outport) = xxx_todo_changeme
        if (yml_link is not None) or self.edge_allowed(inport, outport):
            NX.DiGraph.add_edge(self, inport, outport, ymllink=yml_link)
        # SP: FIXME: Why give yml_link argument if it's None?
        if (yml_link is None) and self.edge_allowed(outport, inport):
            NX.DiGraph.add_edge(self, outport, inport, ymllink=yml_link)

    def add_edges_from(self, connections):
        for connection in connections:
            if len(connection) == 2:
                self.add_edge(connection)
            elif len(connection) == 3:
                inport, outport, yml_link = connection
                self.add_edge((inport, outport), yml_link)

    def full_connect(self, ports):
        return ((p1, p2) for p1 in ports for p2 in ports if p1 != p2)

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <yml-file>" % sys.argv[0]
        sys.exit(2)
    root = parse(sys.argv[1])

    print lxml.objectify.dump(root)
