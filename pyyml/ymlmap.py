import lxml.objectify
import lxml.etree
import os.path
import pyyml
import itertools
import logging
from collections import defaultdict

# setup logger
logger = logging.getLogger(__name__.split('.')[-1])
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


SCHEMA_FILE = os.path.join(os.path.dirname(pyyml.__file__), 'YML_Map.xsd')
NAMESPACE = 'http://sesamesim.sourceforge.net/YML_Map'
NSMAP = {None: NAMESPACE}

# make graph drawing optional
try:
    import pydot
    pydot_available = True
except ImportError:
    pydot_available = False


class YMLEntity(lxml.objectify.ObjectifiedElement):

    def get_source(self):
        return self.get('source')

    def get_dest(self):
        return self.get('dest')

    def set_source(self, source):
        self.set('source', str(source))

    def set_dest(self, dest):
        self.set('dest', str(dest))

    source = property(get_source, set_source)
    dest = property(get_dest, set_dest)


class YMLMultiApp(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}multiapplication' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)


class YMLMapping(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}mapping' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def add_mapping(self, name, side):
        return lxml.etree.SubElement(self,
                                     "{%s}mapping" % NAMESPACE, side=str(side), name=str(name))

    def get_inner_mapping(self):
        '''Get inner mapping environment for adding maps'''
        # get_parent returns None for root node
        parent = self.getparent()
        if parent is not None and parent == '{%s}mapping' % NAMESPACE:
            return self

        child = self.findall(".//{%s}mapping" % NAMESPACE)
        assert len(child) == 1 and self.get_side() != child[0].get_side()
        return child[0]

    def add_instruction(self, source, dest):
        return lxml.etree.SubElement(self,
                                     "{%s}instruction" % NAMESPACE,
                                     source=str(source), dest=str(dest))

    def add_map(self, source, dest, dtmpl=None):
        if dtmpl is None:
            return lxml.etree.SubElement(self, "{%s}map" %
                                         NAMESPACE, source=str(source), dest=str(dest))
        else:
            return lxml.etree.SubElement(self, "{%s}map" %
                                         NAMESPACE, source=str(source), dest=str(dest), dtmpl=str(dtmpl))

    def get_side(self):
        return self.get('side')

    def get_name(self):
        return self.get('name')

    def set_side(self, side):
        self.set('side', str(side))

    def set_name(self, name):
        self.set('name', str(name))

    # This will not work with nested mappings with the same name.
    # FIXME: If we are going to support hierarchical mapping get_map
    # will not be able to work like this. It wil will need a fully
    # qualified path such as "application.network1.task1"
    def get_map(self, comp):
        r = self.xpath("//ns:map[@source='%s']" % comp,
                       namespaces={'ns': NAMESPACE})
        if len(r) == 1:
            return r[0]
        elif len(r) == 0:
            raise NoMappingFound(comp)
        elif len(r) > 1:
            raise MultipleMappingsFound(comp)

    side = property(get_side, set_side)
    name = property(get_name, get_side)


class YMLPort(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}port' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)


class YMLInstruction(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}instruction' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)


class YMLMap(YMLEntity):

    def __init__(self, **args):
        YMLEntity.__init__(self, nsmap=NSMAP)
        self.tag = '{%s}map' % NAMESPACE

        for k, v in args.iteritems():
            self.set(k, v)

    def add_port(self, source, dest):
        return lxml.etree.SubElement(self, "{%s}port" % NAMESPACE,
                                     dest=str(dest), source=str(source))

    def get_dtmpl(self):
        return self.get('dtmpl')

    def set_dtmpl(self, dtmpl):
        self.set('dtmpl', str(dtmpl))

    def get_darg(self):
        return self.get('arg')

    def set_darg(self, val):
        self.set('arg', str(val))

    def toXML(self):
        print "BARFOO"

    dtmpl = property(get_dtmpl, set_dtmpl)
    darg = property(get_darg, set_darg)


class MappingError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class NoMappingFound(MappingError):
    def __init__(self, msg):
        MappingError.__init__(self, msg)


class MultipleMappingsFound(MappingError):
    def __init__(self, msg):
        MappingError.__init__(self, msg)


def parse(filename, validate=True):
    parser = lxml.etree.XMLParser(ns_clean=True, remove_blank_text=True)
    cls_lookup = lxml.etree.ElementNamespaceClassLookup()
    parser.set_element_class_lookup(cls_lookup)

    namespace = cls_lookup.get_namespace(NAMESPACE)
    namespace[None] = YMLEntity
    namespace['multiapplication'] = YMLMultiApp
    namespace['mapping'] = YMLMapping
    namespace['map'] = YMLMap
    namespace['instruction'] = YMLInstruction
    namespace['port'] = YMLPort

    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()

    if validate:
        xmlschema_doc = lxml.etree.parse(file(SCHEMA_FILE))
        xmlschema = lxml.etree.XMLSchema(xmlschema_doc)
        xmlschema.assertValid(tree)

    return tree.getroot()


def new_mapping(source='application', dest='architecture'):
    '''Make new mapping from a specific source to a specific architecture'''
    parser = lxml.etree.XMLParser(ns_clean=True)
    cls_lookup = lxml.etree.ElementNamespaceClassLookup()
    parser.set_element_class_lookup(cls_lookup)

    namespace = cls_lookup.get_namespace(NAMESPACE)
    namespace[None] = YMLEntity
    namespace['multiapplication'] = YMLMultiApp
    namespace['mapping'] = YMLMapping
    namespace['map'] = YMLMap
    namespace['instruction'] = YMLInstruction
    namespace['port'] = YMLPort

    root = parser.makeelement("{%s}mapping" % NAMESPACE,
                              {'side': 'source', 'name': source}, nsmap={None: NAMESPACE})
    root.add_mapping(dest, 'dest')
    return root


def to_dict(mapping, tasks_and_channels=False):
    d = dict()
    for m in mapping.findall(".//{%s}map" % NAMESPACE):
        if tasks_and_channels or not '->' in m.source:
            d[m.source] = m.dest
    return d


def store_mapping(filename, mapping):
    tree = mapping.getroottree()
    logger.debug("store_mapping in file: %s" % filename)
    tree.write(filename, pretty_print=True,
               xml_declaration=True, encoding='UTF-8')


def make_dot(m, arch=None):
    '''Create a simple dot graph of mapping m'''
    if not pydot_available:
        return None

    # Main outermost graph
    graph = pydot.Dot('mapping', graph_type='digraph')
    # graph.set('ranksep', 0.2)

    # Architecture subgraph
    arch_graph = pydot.Subgraph('clusterarchitecture')
    graph.add_subgraph(arch_graph)
    # Application subgraph
    app_graph = pydot.Subgraph('clusterapplication')
    graph.add_subgraph(app_graph)

    # color tasks with the same color as the proc they are mapped on.
    colors = ['red', 'lightblue', 'blue', 'yellow', 'green', 'orange',
              'pink', 'purple']
    color_map = defaultdict(itertools.cycle(colors).next)

    for app_el, arch_el in to_dict(m, True).iteritems():
        if '->' in app_el:
            # Create links from application channel
            src, dst = app_el.split('->')
            src, _ = src.split('.')
            dst, _ = dst.split('.')
            # app_graph.add_edge(pydot.Edge(src, app_el))
            # app_graph.add_edge(pydot.Edge(app_el, dst))
            app_graph.add_edge(pydot.Edge(src, dst))
        else:
            app_graph.add_node(pydot.Node(app_el, style='filled',
                                          fillcolor=color_map[arch_el]))
            arch_graph.add_node(pydot.Node(arch_el, style='filled',
                                           fillcolor=color_map[arch_el]))
            graph.add_edge(pydot.Edge(app_el, arch_el))

    # If an processor is unused it does not appear in the mapping
    # file. If arch is available we can draw all processors, so unused
    # processors also appear in the graph.
    if arch is not None:
        mapped_procs = set(to_dict(m).values())
        all_procs = set([p.name for p in arch.processor_components])
        for proc in all_procs - mapped_procs:
            arch_graph.add_node(pydot.Node(proc, style='filled',
                                           fillcolor='gray'))
    return graph


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <ymlmap-file>" % sys.argv[0]
        sys.exit(2)
    root = parse(sys.argv[1])

    # dot graph of the mapping.
    dot_graph = make_dot(root)
    if dot_graph is not None:
        dot_graph.write_dot('dgraph.dot')
        # print dot_graph.to_string()
    else:
        print "pydot is not installed"

    exit(0)

    print lxml.objectify.dump(root)
    r = new_mapping('application', 'architecture')
    print lxml.objectify.dump(r)

    # Some map creating here
    print
    r1 = r.get_inner_mapping()
    r1.add_map("A", "procA", "mytemplate")
    r1.add_map("Control.Command2-&gt;VLE.Command", "memory", "vmem")
    print lxml.etree.tostring(r.getroottree(), pretty_print=True)

    # SP: test writing
    # store_mapping("test_map.yml", root)

    print "Mapping as dictionary:"
    for k, v in to_dict(root, True).iteritems():
        print "%s - %s" % (k, v)
    print
    print "Mapping as dictionary without channels:"
    for k, v in to_dict(root).iteritems():
        print "%s - %s" % (k, v)
