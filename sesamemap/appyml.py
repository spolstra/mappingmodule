from lxml import etree
from pyyml import yml
from .misc import helperfunctions


class ApplicationYMLLookup(yml.DefaultYMLLookup):

    def property_ancestor(self, element):
        for el in self.ancestors(element):
            if el.tag == '{%s}property' % yml.NAMESPACE:
                return True
        return False

    def lookup(self, document, element):
        if element.tag == '{%s}node' % yml.NAMESPACE and \
                          not self.property_ancestor(element):
            return KahnProcess
        elif element.tag == '{%s}link' % yml.NAMESPACE and \
                            not self.property_ancestor(element):
            return FIFOChannel
        elif element.tag == '{%s}property' % yml.NAMESPACE and \
                element.get('name').startswith('operation:') and \
                not self.property_ancestor(element):
            return Operation
        else:
            return yml.DefaultYMLLookup.lookup(self, document, element)


class KahnProcess(yml.YMLNode):

    def get_required_operations(self):
        try:
            return set(eval(self.get_property('operations')[0].value))
        except Exception:
            return set()

    required_operations = property(get_required_operations)

    def __cmp__(self, o):
        return cmp(self.name, o.name)

    def __repr__(self):
        return self.name

    def __str__(self):
        return 'Kahn process %s' % self.name


class FIFOChannel(yml.YMLLink):
    processes = yml.YMLLink.nodes

    def __repr__(self):
        return str(self)


class Operation(yml.YMLProperty):

    def get_opid(self):
        return int(self.get('value'))

    def get_name(self):
        return self.get('name')[10:]

    opid = property(get_opid)
    name = property(get_name)

    def __str__(self):
        return "operation %d: %s" % (self.opid, self.name)

    def __cmp__(self, o):
        return cmp(self.opid, o.opid)


class Application(yml.YMLGraph):

    def __init__(self, filename, **kwds):
        app_yml = yml.parse(filename, ApplicationYMLLookup)
        yml.YMLGraph.__init__(self, app_yml, data=None, name=None, **kwds)
        self.name = "KPN %s" % self.yml.name
        self.processes = sorted(
            n for n in self.yml.xpath("//ns:node",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(n, KahnProcess))
        self.channels = sorted(
            l for l in self.yml.xpath("//ns:link",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(l, FIFOChannel))
        self.operations = sorted(
            p for p in self.yml.xpath("//ns:property",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(p, Operation))
        self.kpn_elements = self.processes + self.channels
        self.add_edges_from(e.ports + (e,) for e in self.channels)
        # Mapped_elements are used in design space exploration.
        self.mapped_elements = self.kpn_elements

    def write(self, filename, pretty=True):
        tree = self.yml.getroottree()
        tree.write(filename, pretty_print=pretty)

    def add(self, element):
        '''Add the element to the application'''
        self.yml.append(element)

        if isinstance(element, KahnProcess):
            self.processes.append(element)
        elif isinstance(element, Operation):
            self.operations.append(element)
        elif isinstance(element, FIFOChannel):
            self.channels.append(element)
        else:
            raise Exception("Invalid element type")

    def delProcess(self, process):
        '''Remove the specified process from this application'''
        assert process in self.processes
        self.yml.remove(process)
        self.processes.remove(process)

    def addProcess(self, name, classname):
        '''Add new process to application with specific name and direction'''
        p = etree.SubElement(self.yml, '{%s}node' % (yml.NAMESPACE))
        p.set('name', name)
        p.set('class', classname)

        self.processes.append(p)
        return p

    def addPort(self, process, name, dir):
        '''Add port to process with specific name and direction'''
        assert process in self.processes
        assert dir in ['in', 'out']
        p = etree.SubElement(process, '{%s}port' % (yml.NAMESPACE))
        p.set('name', name)
        p.set('dir', dir)

        return p

    def addLink(self, inport, outport):
        # Add YML link
        lnk = etree.SubElement(self.yml, '{%s}link' % (yml.NAMESPACE))
        lnk.set('innode', inport.getparent().name)
        lnk.set('inport', inport.name)
        lnk.set('outnode', outport.getparent().name)
        lnk.set('outport', outport.name)
        return lnk

    def table(self):
        header = ('Index', 'Kahn process/channel name')
        data = [header] + list(enumerate(self.processes + self.channels))
        return helperfunctions.columns(data)

    def channels_outgoing(self, process):
        '''Return the set of FIFO channels incoming to process.'''
        return self.yml.findall(".//{%s}link[@innode='%s']" %
                                (yml.NAMESPACE, process.name))

    def channels_incoming(self, process):
        '''Return the set of FIFO channels outgoing from process.'''
        return self.yml.findall(".//{%s}link[@outnode='%s']" %
                                (yml.NAMESPACE, process.name))

    def connected_channels(self, process):
        '''Return the set of FIFO channels incoming to and outgoing from
        process.'''
        return self.channels_outgoing(process) + \
            self.channels_incoming(process)

    def get_task_ids(self):
        tids = dict()
        try:
            for task in self.processes:
                tids[task.name] = int(task.get_property('taskid')[0].value)
        except IndexError:
            return dict()
        return tids

    def operation(self, opid):
        '''Return the Operation object that has opid as its operation id.'''
        # current implementation is O(n) with n = len(self.operations)
        # this can be done faster if required, using binary search
        for o in self.operations:
            if o.opid == opid:
                return o
        return None


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <app_yml-file>" % sys.argv[0]
        sys.exit(2)
    app = Application(sys.argv[1])
    print app
    print app.operations
    print app.table()
    for o in app.operations:
        print o
    for p in app.processes:
        print p, app.channels_outgoing(p)
    for p in app.processes:
        print p, app.channels_incoming(p)
    for i in xrange(10):
        print i, app.operation(i)
    print "Application channels"
    print app.channels
    app.write("app_copy.yml", True)
