from .appyml import Application, KahnProcess, FIFOChannel
from .archyml import Architecture, Processor, Memory
import random
import logging
from .misc import helperfunctions
from pyyml import ymlmap

# setup logger
logger = logging.getLogger(__name__.split('.')[-1])
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class AppArchMap(dict):

    def __init__(self, app, arch, *args, **kwargs):
        self.app = app
        self.arch = arch
        dict.__init__(self, *args, **kwargs)
        if set(self.app.processes + self.app.channels) != set(self.iterkeys()):
            # If our AppArchMap was not already initialized via args
            # or kwargs, initialise it with a random mapping.
            self.randomize()

    def to_chromosome(self):
        """Used for printing the chromosome in logging/debug messages"""
        gene_order = self.app.mapped_elements
        arch_index = self.arch.components_index
        try:
            r = [arch_index[self[n]] for n in gene_order]
        except KeyError:
            # It can happen that channels are not mapped, as we can use
            # repair to add random channel mappings and we can be called
            # from repair.
            # In this case we return -1 for channel dest. Is OK as this
            # method is only for debug printing.
            r = [arch_index[self[n]] for n in self.app.processes] + \
                    [-1 for _ in self.app.channels]
        return r

    def reverse_map(self):
        revmap = dict((v, set()) for v in self.arch.components)
        for app_node, arch_comp in self.iteritems():
            revmap[arch_comp].add(app_node)
        return revmap

    def feasible_mappings(self, app_node, limited_proc_set=None):
        if isinstance(app_node, KahnProcess):
            # Find the set of processors that are able to run app_node
            # (i.e., a processor that can run all operations executed by the
            # process).
            # Check if we need to use a subset of the available procs.
            logger.debug('limit set: %s' % limited_proc_set)
            if limited_proc_set is None:
                proc_set = self.arch.processors
            else:
                proc_set = limited_proc_set
            return set(p for p in proc_set if
                       app_node.required_operations <= p.allowed_operations)
        elif isinstance(app_node, FIFOChannel):
            kpn_src, kpn_dest = app_node.processes
            p_src = self[kpn_src]
            # Some processors do not support internal communication,
            # it is possible to map two or more processes on these kind
            # of processors, but if these processes want to communicate, the
            # channel should be mapped onto an external memory.
            p_dest = self[kpn_dest]
            # p_src is the  architecture component of the source side of
            # the fifochannel.  And p_dest the component of the dest side.
            if p_src == p_dest and 'vself' in p_src.get_templates():
                # If both channel endings (processes) are mapped onto the same
                # processor, this channel must also be mapped onto that
                # processor.
                # Since both processes are mapped on the same processor, this
                # processor is the only element of the feasible set.
                # There could be an architecture platforms where
                # task A and B are mapped into a processor X, but a
                # channel c between A and B still needs to be mapped
                # onto an external memory component.
                # This is why we return the union of both.
                return set([p_src]).union(self.arch.reachable_memories(
                    p_src, p_dest))
            else:
                # If both channel endings (processes) are not mapped onto the
                # same processor, this channel must be mapped onto a reachable
                # memory component.
                # Therefore, the feasible set of this channel is now the set of
                # reachabele memory components.
                return self.arch.reachable_memories(p_src, p_dest)
        else:
            return set()

    def __setitem__(self, app_node, arch_node):
        if arch_node in self.feasible_mappings(app_node):
            dict.__setitem__(self, app_node, arch_node)
        else:
            raise Exception("not allowed: %s->%s" % (app_node, arch_node))

    def is_mapped(self, app_node, arch_node):
        return self[app_node] == arch_node

    def is_valid(self):
        """Validate the current mapping."""
        logging.debug("entered is_valid")
        for process in self.app.processes:
            logging.debug("task: {}, proc: {}, feasible: {}".format(
                process, self[process], self.feasible_mappings(process)))
            if self[process] not in self.feasible_mappings(process):
                return False
        for channel in self.app.channels:
            logging.debug("channel: {}, proc: {}, feasible: {}".format(
                channel, self[channel], self.feasible_mappings(channel)))
            if self[channel] not in self.feasible_mappings(channel):
                return False
        return True

    def repair(self, limited_set=None):
        """Repair this mapping.

        An AppArchMap represents a mapping of an Application to an
        Architecture as a dictionary.
        Such a mapping can represent a situation that is invalid if:

        * An application task is mapped onto an processor that cannot
          execute that task.
        * Assume task A is mapped onto processor X and task B is mapped onto
          processor Y, a channel c between A and B needs to be mapped
          onto a component Z that is connected to processors X and Y.
          If Z is not reachable from X or from Y, the mapping is invalid.

        We repair an invalid mapping with the following steps:

        1. Check if all tasks are mapped onto processors that can support
           them.  If not: randomly select a processor from the set of
           supported processors.
        2. Check if all channels have at least one mapping option that is
           valid.  If not: remapped one of the tasks of the channel
           to a random processor, and check again.

        Algorithm in pseudo code:
        for p in processes:
            Set feasible to true if mapping of p is valid, false otherwise.
            Mark p as 'seen'.
            if feasible is true:
                for c in all_channels:
                    if tasks (a,b) of channel c have been checked/seen:
                        if c has no feasible mappings with current (a,b)
                        mapping:
                            feasible = false

            if feasible is false:
                There are channels without mapping options or p mapping is
                invalid.
                while true:
                    map p onto randomly chosen feasible component.
                    check if 'seen' channels have feasible sets with this
                    new mapping.
                    [a channel is 'seen' if both it tasks are checked and
                    have good mappings]
                    [only look at channels that have both tasks already
                    checked]
                    [optimisation possibility: only check c == (p,x)||(x,p)]
                    if all seen channels have at least 1 feasible mapping
                    option:
                        break out of while-true loop.
            All p's so far have good mappings, and all 'seen' channels
            have at least one feasible mapping option.

        At this point all p's (tasks) are mapped, and all channels can be
        mapped with this current task mapping. So now map the channels.
        for c in channels:
            if mapping of c is not valid:
                Randomly pick a mapping for c from its feasible set.

        Now all tasks and channels are correctly mapped.
        """
        logger.debug("Enter repair mapping: {}".format(self.to_chromosome()))

        seen = set()
        for process in self.app.processes:
            logger.debug("Checking process: %s" % process)
            feasible_set = self.feasible_mappings(process, limited_set)
            logger.debug(process)
            logger.debug(self)
            # First check task mapping
            feasible = self[process] in feasible_set
            seen.add(process)
            # And then if channels are feasible so far.
            if feasible:
                for c in self.app.channels:
                    logger.debug("channel: %s" % c)
                    # Check if both tasks have already been
                    # mapped. Otherwise we ignore it.
                    (a, b) = c.processes
                    if a in seen and b in seen:
                        logger.debug("Feasible mappings for %s: %s" %
                                     (c, self.feasible_mappings(c)))
                        if not self.feasible_mappings(c):
                            feasible = False
                            break

            # Not feasible if task in not in the feasible set or this
            # task mapping makes current channel mappings invalid.
            if not feasible:
                # Pick random element from feasible set
                # FIXME: In theory this could be an infinite loop.
                # See notes 12-4 page 38 for an example.
                # See also footnote 2 in the paper:
                # "Multiobjective optimization and evolutionary
                # algorithms for the application mapping problem in
                # multiprocessor system-on-chip design"
                # by Erbas, Cerav-Erbas & Pimentel
                logger.debug("Mapping not feasible, fixing..")
                while True:
                    self[process] = random.choice(
                        sorted(feasible_set, limited_set))
                    logger.debug("Trying %s -> %s" % (process,
                                                      self[process]))

                    # Check all channels c to see if this pick will cause
                    # empty feasible sets for channels. If so we randomly
                    # pick another
                    feasible = True
                    logger.debug("Checking channel mappings")
                    for c in self.app.channels:
                        logger.debug("Checking: %s" % c)
                        # Check if both tasks have already been
                        # mapped. Otherwise we ignore it.
                        (a, b) = c.processes
                        if a in seen and b in seen:
                            if not self.feasible_mappings(c):
                                logger.debug("Empty feasible set: %s" % c)
                                feasible = False
                                break
                    if feasible:  # No empty feasible sets found
                        break

        logger.debug("Task repair done. all channels have mapping options.")
        # At this point we can be sure that the set of feasible
        # mappings is not empty for every application channel.
        # Note: Multiple application channels can be mapped onto a single
        # architecture fifo. See notes p.98.
        for channel in self.app.channels:
            feasible_set = self.feasible_mappings(channel)
            try:
                if self[channel] not in feasible_set:
                    self[channel] = random.choice(sorted(feasible_set))
                    logger.debug("%s mapped to %s" % (channel, self[channel]))
            except KeyError:
                # If channel has not been set yet.
                self[channel] = random.choice(sorted(feasible_set))
                logger.debug("%s mapped to %s" % (channel, self[channel]))

        logger.debug("Exit repair mapping: {}".format(self.to_chromosome()))

    def randomize(self, limit=None):
        """Generate a random, valid Application->Architecture mapping."""
        limited_set = None
        if limit is not None:
            logger.debug("Randomize mapping, limit=%s" % limit)
            # If there is a limit, first map all tasks onto one proc.
            proc = random.choice(sorted(self.arch.processors))
            for process in self.app.kpn_elements:
                self[process] = proc

        logger.debug("Randomize task mapping")
        for process in self.app.processes:
            # First check if we reached the limit
            if limit is not None:
                current_procs = [comp for comp in self.all_mapped() if
                                 isinstance(comp, Processor)]
                if len(current_procs) == limit:
                    # Limit reached, now only select procs from the
                    # current limit set.
                    limited_set = current_procs
            self[process] = random.choice(
                sorted(self.feasible_mappings(process, limited_set)))

        # FIXME: With unreachable combinations, the list of feasible
        # channels might be empty.

        logger.debug("Check if channels have only one mapping option")
        for channel in self.app.channels:
            mapping_options = list(self.feasible_mappings(channel))
            if len(mapping_options) == 1:
                self[channel] = mapping_options[0]

        if self.is_valid():
            logger.debug("channel mappings are valid")
        else:
            # Call repair method to assign random feasible channel mappings.
            logger.debug("Get channel mappings by calling repair mapping")
            self.repair(limited_set)


    def get_dtmpl(self, app_node, arch_node):
        """Calculate default template mapping when mapping app_node onto
        arch_node."""
        if isinstance(app_node, KahnProcess):
            # process mapped onto a processor == vproc
            dtmpl = 'vproc'
        elif isinstance(app_node, FIFOChannel) and isinstance(
                arch_node, Processor):
            # channel mapped onto a processor == vself
            dtmpl = 'vself'
        elif isinstance(app_node, FIFOChannel) and isinstance(
                arch_node, Memory):
            # channel mapped onto a memory == first template of memory
            dtmpl = arch_node.get_templates().keys()[0]
        else:
            raise Exception("impossible mapping")
        return dtmpl

    def all_mapped(self):
        """Return the set of architecture components used in this mapping"""
        return sorted(set(self.values()))

    def __str__(self):
        header = ('Kahn process/channel', 'Architecture component', 'Template')
        data = []
        for app_node, arch_node in self.iteritems():
            dtmpl = self.get_dtmpl(app_node, arch_node)
            data.append((str(app_node), str(arch_node), dtmpl))
        data.sort()
        data = [header] + data
        return helperfunctions.columns(data, "%%-%ds ==> %%-%ds %%-%ds")

    def export_yml(self):
        yml = ymlmap.new_mapping()
        yml.set_name(self.app.yml.name)
        yml.mapping.set_name(self.arch.yml.name)
        # instructions should be first
        for op in self.app.operations:
            yml.mapping.add_instruction(op.name, op.name)
        for app_node, arch_node in self.iteritems():
            src = (
                app_node.name if isinstance(
                    app_node,
                    KahnProcess) else str(
                        app_node))
            dtmpl = self.get_dtmpl(app_node, arch_node)
            yml.mapping.add_map(src, arch_node.name, dtmpl)
        return yml


class IterMappings:

    def __init__(self, app, arch):
        self.app = app
        self.arch = arch
        self.fsm = {}
        for process in self.app.processes:
            self.fsm[process] = set(
                p for p in self.arch.processors if process.required_operations <= p.allowed_operations)

    def __processor_mappings(self):
        return helperfunctions.cartesian_product(*self.fsm.values())

    def channel_feasible(self, channel, mapping):
        s, d = channel.processes
        msource, mdest = mapping[s], mapping[d]
        if msource == mdest and 'vself' in msource.get_templates():
            return set([msource])
        return self.arch.reachable_memories(msource, mdest)

    def __len__(self):
        isum = 0
        for pm in self.__processor_mappings():
            pmapping = dict(zip(self.fsm.keys(), pm))
            feasible_sets = (len(self.channel_feasible(p, pmapping))
                             for p in self.app.channels)
            isum += helperfunctions.product(feasible_sets)
        return isum

    def __iter__(self):
        for pm in self.__processor_mappings():
            pmapping = dict(zip(self.fsm.keys(), pm))
            feasible_sets = [self.channel_feasible(p, pmapping)
                             for p in self.app.channels]
            channel_mappings = helperfunctions.cartesian_product(
                *feasible_sets)
            for cm in channel_mappings:
                cur_mapping = dict(
                    zip(self.fsm.keys() + self.app.channels, pm + cm))
                cur_mapping = AppArchMap(self.app, self.arch, cur_mapping)
                assert cur_mapping.is_valid(), "generated an invalid mapping"
                yield cur_mapping


if __name__ == "__main__":
    import sys
    import time

    #logger.setLevel(logging.INFO)
    logger.setLevel(logging.DEBUG)

    if len(sys.argv) != 3:
        print "Usage: apparchmap.py <app> <arch>"
        sys.exit(2)

    random.seed(1)
    logger.debug("start unit test")

    logger.info("Reading application...")
    t_start = time.time()

    app = Application(sys.argv[1])

    t_total = time.time() - t_start
    logger.info('Constructing application took %f s' % t_total)
    t_start = time.time()

    # Test path length limit of 25
    logger.info("Reading architecture...")
    arch = Architecture(sys.argv[2], True, 25)

    t_total = time.time() - t_start
    logger.info('Constructing Architecture with DFA took %f s' % t_total)
    t_start = time.time()

    logger.info("Get apparchmap object m")
    m = AppArchMap(app, arch)

    t_total = time.time() - t_start
    logger.info('Constructing Mapping took %f s' % t_total)
    t_start = time.time()

    print "procs: %s" % arch.processors
    print "mems: %s" % arch.memories
    print "apparchmap: %s" % m
    for x, y in ((x, y) for x in app.processes + app.channels for y in arch.processors + arch.memories):
        print 'map', x, '=>', y,
        try:
            m[x] = y
            print 'dtmpl =', m.get_dtmpl(x, y)
        except:
            print '==== not allowed'

    logger.info("Check validity of m")
    print m.is_valid()
    yml = m.export_yml()
    ymlmap.store_mapping('testmap.yml', yml)

    logger.info("Construct AppArchMap with given mapping.")
    d = [(task, arch.components[0]) for task in app.processes +
         app.channels]
    m2 = AppArchMap(app, arch, d)
    print "procs: %s" % arch.processors
    print "mems: %s" % arch.memories
    print "apparchmap: %s" % m

    for x in app.processes:
        print x, x.required_operations

    for x in arch.processors:
        print x, x.allowed_operations

    for x in app.processes:
        print "Feasible task options", x, m.feasible_mappings(x)

    for x in app.channels:
        print "Feasible channel options:", x, m.feasible_mappings(x)

    # Iterating all mappings will take to long for most architectures.
    im = IterMappings(app, arch)
    for i, m in enumerate(im):
        print i, m.is_valid()
        ymlm = m.export_yml()
        # ymlmap.store_mapping('map%d.yml' % i, yml)
        dot_graph = ymlmap.make_dot(ymlm)
        dot_graph.write_dot('map%d.dot' % i)
        if i > 20:
            break
    # print len(im)

    if (False):
        # Generate NUM_MAP random mappings
        NUM_MAP = 20
        print 'generating %d random mappings' % NUM_MAP
        t_start = time.time()
        for x in xrange(NUM_MAP):
            m = AppArchMap(app, arch)
            assert m.is_valid(), "invalid mapping generated"
            yml = m.export_yml()
            ymlmap.store_mapping('map%d.yml' % x, yml)

            sys.stdout.write("\r%d%%" % int((float(x + 1) / NUM_MAP) * 100))
            sys.stdout.flush()
        print

        t_total = time.time() - t_start
        print 'done %f s, %f mappings per second' % (
            t_total, NUM_MAP / t_total)
