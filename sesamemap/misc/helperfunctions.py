
def columns(data, print_template=None):
    n_columns = len(data[0])
    column_sizes = [0] * n_columns
    if not print_template:
        print_template = "%%-%ds  " * n_columns
    # get largest width of each column
    for entry in data:
        for i, column_size in enumerate(column_sizes):
            cur_entry = len(str(entry[i]))
            if cur_entry > column_sizes[i]:
                column_sizes[i] = cur_entry
    print_template = print_template % tuple(column_sizes)
    return '\n'.join(print_template % entry for entry in data)

if __name__ == "__main__":
    import random

    def data(entries):
        retval = []
        c = 0
        while c != entries:
            t = ('a' * random.randint(1, 10), 'b' *
                 random.randint(1, 10), 'c' * random.randint(1, 10))
            retval.append(t)
            c += 1
        return retval

    print columns(data(4))
    print columns(data(4), "%%-%ds ==> %%-%ds %%%ds")


def nary_cart_expr(n):
    t = ','.join("x%d" % i for i in xrange(n))
    f = ' '.join("for x%d in s[%d]" % (i, i) for i in xrange(n))
    return "((%s,) %s)" % (t, f)


def cartesian_product(*l):
    if not len(l):
        return []
    return eval("lambda s: %s" % nary_cart_expr(len(l)))(l)


def product(l):
    '''Returns the product of a sequence of numbers (NOT strings).'''
    if l == []:
        return 0
    x = 1
    for y in l:
        if not y:
            return 0
        x *= y
    return x
