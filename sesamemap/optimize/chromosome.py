import random
import logging
from sesamemap.apparchmap import AppArchMap
from sesamemap.misc import helperfunctions
from pyyml import ymlmap
from sesamemap.appyml import Application
from espam.espamapp import Application as EspamApplication
from espam.apparchmap import AppArchMap as EspamAppArchMap

# Copy from mappingmodule library
# remove custom GA code.
# reuse mutate/crossover and conversion functions


def onepoint_crossover(p1, p2, n_channels=0):
    '''Performs in-place one-point crossover of p1 and p2.
    Crossover is done upto the crossover_end index. This exempts the channel
    mappings from being part of the crossover.'''
    if n_channels == 0:
        crossover_end = len(p1) #  OK, Our chromosomes are same length
    else:
        crossover_end = len(p1) - n_channels  # stop x-over at channel genes
    crossover_point = random.randint(0, crossover_end)
    o1 = list(p1)  # save p1
    p1[crossover_point:crossover_end] = p2[crossover_point:crossover_end]
    p2[crossover_point:crossover_end] = o1[crossover_point:crossover_end]


def mutate(q, rand_values, n_channels, p=0.2):
    if n_channels == 0:  # mutate both tasks and channel mappings
        for i in xrange(len(q)):
            if random.random() < p:
                q[i] = random.choice(rand_values)
    else:  # only mutate task mapping, leave channel mappings alone.
        for i in xrange(len(q) - n_channels):
            if random.random() < p:
                q[i] = random.choice(rand_values)


def chromosome2apparchmap(app, arch, chromosome):
    gene_order = app.mapped_elements
    allele_order = arch.components
    apparchmap = {}
    for ni, ci in enumerate(chromosome):
        # Convert the indices to the task and component names.
        n = gene_order[ni]
        c = allele_order[ci]
        # Store the mapping of task n onto component c.
        apparchmap[n] = c
    if isinstance(app, Application):
        logging.debug("chromosome2apparchmap returns YML apparchmap")
        return AppArchMap(app, arch, apparchmap)
    elif isinstance(app, EspamApplication):
        logging.debug("chromosome2apparchmap returns Espam apparchmap")
        return EspamAppArchMap(app, arch, apparchmap)


def apparchmap2chromosome(apparchmap):
    gene_order = apparchmap.app.mapped_elements
    arch_index = apparchmap.arch.components_index
    return (arch_index[apparchmap[n]] for n in gene_order)


def processing_time(c):
    pass


def power_usage(c):
    pass


def cost(c):
    pass


class SesameMapChromosome(list):
    objectives = (processing_time, power_usage, cost)

    def __init__(self, app, arch, *args, **kwargs):
        self.app = app
        self.arch = arch
        self.gene_order = self.app.kpn_elements
        self.allele_order = self.arch.components
        self.arch_index = self.arch.components_index

        if 'chromosome' in kwargs:
            chromosome = kwargs['chromosome']
        else:
            chromosome = self.random(app, arch)

        list.__init__(self, chromosome)

    def show_gene_table(self):
        header = ('Index', 'Application process/channel')
        data = [header] + [(i, n) for i, n in enumerate(self.gene_order)]
        header = ('Index', 'Architecture component')
        data += [header] + [(i, c) for i, c in enumerate(self.allele_order)]
        return helperfunctions.columns(data)

    def apparchmap(self):
        return chromosome2apparchmap(self.app, self.arch, self)

    def feasible(self):
        return self.apparchmap().is_valid()

    def new_chromosome(self, chromosome):
        args = self.__getnewargs__()
        kwargs = {'chromosome': chromosome}
        return self.__class__(*args, **kwargs)

    def __getnewargs__(self):
        return (self.app, self.arch)

    @staticmethod
    def random(app, arch):
        apparchmap = AppArchMap(app, arch)
        return apparchmap2chromosome(apparchmap)

if __name__ == "__main__":
    import sys
    from sesamemap.archyml import Architecture
    if len(sys.argv) != 3:
        sys.exit(2)

    app = Application(sys.argv[1])
    arch = Architecture(sys.argv[2])
    print range(len(arch.components))
    aam = AppArchMap(app, arch)
    print "testing init chromos"
    print aam
    print list(apparchmap2chromosome(aam))
    print "end init"

    # this is useful for our dse
    c = SesameMapChromosome(app, arch, chromosome=[1, 3, 0])
    c1 = apparchmap2chromosome(c)
    print c1
    print type(c1)
    print "print c"
    print c
    print list(c)
    print type(list(c))
    print
    # m = chromosome2apparchmap(app, arch, c)
    m = chromosome2apparchmap(app, arch, list(c))
    print "check on apparchmap directly"
    print m.is_valid()

    print m
    print c.feasible()
    m.repair()
    print m
    c2 = apparchmap2chromosome(m)
    print list(c2)
    # -- upto here

    # start with new m
    m = SesameMapChromosome(app, arch, chromosome=[1, 3, 0])
    print m.show_gene_table()

    print chromosome2apparchmap(app, arch, m)
    print "copy chromo"
    m = m.new_chromosome(m.random(app, arch))
    print "new chromo:"
    print m
    print m.show_gene_table()
    print "convert to map"
    print chromosome2apparchmap(app, arch, m)
    print type(m)
    print m.feasible()  # <---------------------
    print "Copy that chromo"
    m2 = m.new_chromosome(m)
    print m2
    print chromosome2apparchmap(app, arch, m2)

    print "repair test"
    map1 = chromosome2apparchmap(app, arch, m2)
    print "1"
    map1.repair()
    print "2"
    m2_new = apparchmap2chromosome(map1)
    print "3"
    print chromosome2apparchmap(app, arch, m2_new)

    print "*** mutate test"
    print m2
    mutate(m2, [2, 2, 2, 2])
    print m2
    print m2.feasible()
    print chromosome2apparchmap(app, arch, m2)
    print "*** repair test"
    torepair = chromosome2apparchmap(app, arch, m2)
    torepair.repair()
    m2_new = m2.new_chromosome(apparchmap2chromosome(torepair))
    print chromosome2apparchmap(app, arch, m2_new)
    print type(m2_new)
    print m2_new.feasible()

    print "*** simple one point crossover"
    a = [1] * 4
    b = [2] * 4
    print a
    print b
    onepoint_crossover(a, b)
    print a
    print b

    print "*** real one point crossover"
    c1 = SesameMapChromosome(app, arch)
    c2 = SesameMapChromosome(app, arch)
    print c1
    print c2
    onepoint_crossover(c1, c2)
    print "crossover:"
    print c1
    print c1.feasible()
    print c2
    print c2.feasible()
    print "repair c1"
    t = chromosome2apparchmap(app, arch, c1)
    t.repair()
    c1_new = c1.new_chromosome(apparchmap2chromosome(t))
    print c1_new
    print c1_new.feasible()
    print "Write to file 'testmap.yml'"
    map_yml = t.export_yml()
    ymlmap.store_mapping('testmap.yml', map_yml)

    # optimal_mappings = nsga2.nsga2(3,10,SesameMapChromosome,((app,arch),{}))
    # for i, p in enumerate(optimal_mappings):
    # print i, chromosome2apparchmap(app, arch, p), tuple(m(p) for m in
    # SesameMapChromosome.objectives)
