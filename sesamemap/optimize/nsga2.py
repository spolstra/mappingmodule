"""Multi-objective optimization using the NSGA-II algorithm.

This module implements the NSGA-II algorithm as described in:

@article{deb2002fae,
  title={{A fast and elitist multiobjective genetic algorithm: NSGA-II}},
  author={Deb, K. and Pratap, A. and Agarwal, S. and Meyarivan, T.},
  journal={Evolutionary Computation, IEEE Transactions on},
  volume={6},
  number={2},
  pages={182--197},
  year={2002}
}

User-specified chromosomes used in the optimization process should inherit
from the Individual class.

"""
from functools import wraps

__all__ = ['Individual', 'nsga2']


def memoize(f, cache):
    """Cache the result of function calls."""
    @wraps(f)
    def cache_wrapper(x):
        if (f, x) not in cache:
            cache[(f, x)] = f(x)
        return cache[(f, x)]
    return cache_wrapper


class Individual(tuple):

    """Subclassable chromosome type used by NSGA-II.

    Users must subclass this class and at least implement the static
    random() method, the static make_offspring() method, and update the
    class's objectives attribute.
    The objectives attribute must be a tuple with one or more references
    to functions that take the subclassed Individual as an input.

    """
    objectives = tuple()

    def __new__(self, *args, **kwargs):
        if 'chromosome' in kwargs:
            chromosome = kwargs['chromosome']
        else:
            chromosome = self.random(*args, **kwargs)
        t = tuple.__new__(self, chromosome)
        t._rank = 0
        t._distance = 0.0
        return t

    def __init__(self, *args, **kwargs):
        self._objectives_value = dict((m, m(self)) for m in self.objectives)

    def __getnewargs__(self):
        return ()

    def dominates(self, o):
        # TODO: check constraints, using the feasible() method
        for m in self.objectives:
            if self._objectives_value[m] > o._objectives_value[m]:
                return False
        return True

    def __cmp__(self, o):
        """Compare two Individuals using Crowded-Comparison.

        Do *not* override this method, as its implementation is crucial
        for a correctly working optimization procedure.

        """
        return (1 if self._rank < o._rank or
                (self._rank == o._rank and self._distance > o._distance)
                else -1)

    def new_chromosome(self, chromosome):
        args = self.__getnewargs__()
        kwargs = {'chromosome': chromosome}
        return self.__class__(*args, **kwargs)

    @staticmethod
    def random(*args, **kwargs):
        """Create a random chromosome.

        Override this method. It should return a random iterable.
        Do not forget to set the staticmethod decorator.
        For example:
        return (random.randint(0,100) for x in xrange(10))

        """
        raise NotImplementedError(
            "You must implement this as a static method. It should return a random iterable.")

    def feasible(self):
        return True

    @staticmethod
    def make_offspring(P):
        """Generate offspring population from parent population.

        Override this method. It should return a set Q with size |P|.
        Do not forget to set the staticmethod decorator.

        """
        raise NotImplementedError(
            "You must implement this as a static method. It should return a set Q with size |P|.")


def _fast_nondominated_sort(P):
    """Sort a population into different nondomination levels."""
    S = {}
    n = {}
    # Fronts, with F[0] the first front, etc.
    F = [set()]
    for p in P:
        S[p] = set()  # Solutions dominated by p
        n[p] = 0  # Number of solutions dominating p
        for q in P:
            if p == q:
                continue
            if p.dominates(q):
                S[p].add(q)
            elif q.dominates(p):
                n[p] += 1
        if not n[p]:
            # p belongs to the first front
            p._rank = 0
            F[0].add(p)
    while F[-1]:
        Q = set()  # Will contain elements of the next front
        for p in F[-1]:
            for q in S[p]:
                n[q] -= 1
                if not n[q]:
                    q._rank = len(F)
                    Q.add(q)
        # Current front is formed with all members of Q
        F.append(Q)
    return F


def _crowding_distance_assignment(L):
    """Calculate the crowding distance for diversity preservation."""
    if not L:
        return
    for i in L:
        i._distance = 0.0
    for m in i.objectives:
        # L is sorted by cost evaluation using m.
        # This should be done in parallel first, and the cached result will be
        # used.
        L = sorted(L, key=lambda i: i._objectives_value[m])
        L[0]._distance = L[-1]._distance = float('inf')
        f_max = L[-1]._objectives_value[m]
        f_min = L[0]._objectives_value[m]
        if f_max != f_min:
            for i in xrange(1, len(L) - 2):
                m_succ = L[i + 1]._objectives_value[m]
                m_pred = L[i - 1]._objectives_value[m]
                L[i]._distance += (m_succ - m_pred) / (f_max - f_min)


def nsga2(N, T, individual, ind_options=((), {})):
    """Run the NSGA-II algorithm for multi-objective optimization.

    N: Population size
    T: Maximum number of generations
    individual: the type of a user-specified subclass of Individual

    ind_options: a tuple (*args, **kwargs) with optional arguments
    that are used as arguments to instantiate a new individual.

    """
    # All functions get the memoize decorator in order to cache function
    # evaluations.
    cache = {}
    individual.objectives = tuple(memoize(m, cache)
                                  for m in individual.objectives)
    R = set()
    individual_args, individual_kwargs = ind_options
    while len(R) < 2 * N:
        ind = individual(*individual_args, **individual_kwargs)
        R.add(ind)
    t = 0
    while t < T:
        print t
        # F = (F_1, F_2, ...) all non-dominated fronts of R
        F = _fast_nondominated_sort(R)
        P = set()
        i = 0
        while i + 1 < len(F) and len(P) + len(F[i]) <= N:
            _crowding_distance_assignment(F[i])
            P.update(F[i])
            i += 1
        F = sorted(F[i], reverse=True)
        P.update(F[:N - len(P)])
        Q = individual.make_offspring(P)
        # Combine parent and children population
        R = P | Q
        t += 1
    return P
