import numpy


def get_signature(p):
    s = p.get_property('signature')
    if s:
        e = eval(s[0].value)
        if isinstance(e, list) and len(e):
            return e
    return None


def calc_objectives(mapping):
    V_K = mapping.app.processes
    E_K = mapping.app.channels
    P = mapping.arch.processors
    M = mapping.arch.memories
    rev_map = mapping.reverse_map()
    signatures = {}
    for n in V_K + E_K + P + M:
        signatures[n] = get_signature(n)
    performances = {}
    power_usage = {}
    cost = {}
    for p in P:
        # set of application processes mapped on this processor
        processes = rev_map[p] & set(V_K)
        # xt = 1/c with c the computational capacity of the processor
        xt, xp, xc = signatures[p]
        if not processes:
            perf_execution = 0
        else:
            alpha = sum(numpy.array(signatures[kp])
                        for kp in processes if signatures[kp] is not None)
            xt = numpy.array(xt)
            perf_execution = (
                numpy.dot(alpha,
                          xt) if isinstance(alpha,
                                            type(xt)) else 0)
        # Each process that is mapped on this processor, may communicate with
        # processes that are mapped onto other processors. In that case, the
        # FIFO channel that connects these processes is mapped onto an external
        # memory. This is also the case when the processor does not support
        # internal communication. In both cases, communication comes with a
        # certain cost.
        # channels mapped onto this processor
        channels = rev_map[p] & set(E_K)
        # For each process mapped on this processor, check which incoming or
        # outgoing FIFO channel is mapped onto an external memory.
        perf_communication = 0
        for process in processes:
            external_channels = mapping.app.connected_channels(
                process) - channels
            # For each channel mapped on an external memory, add communication
            # cost. This cost depends on the number and size of tokens
            # transmitted over this channel.
            for c in external_channels:
                n_tokens = signatures[c][0]
                n_size = signatures[c][1]
                bytes = float(n_tokens * n_size)
                m = mapping[c]
                r_read = signatures[m][0][0]
                r_write = signatures[m][0][1]
                if c.nodes[1] in processes:
                    # c is an incoming channel
                    perf_communication += bytes / r_read
                elif c.nodes[0] in processes:
                    # c is an outgoing channel
                    perf_communication += bytes / r_write
        performances[p] = perf_execution + perf_communication
        power_usage[p] = perf_execution * xp[0] + perf_communication * xp[1]
        cost[p] = xc[0] * bool(rev_map[p])

    for m in M:
        # channels mapped onto this memory
        channels = rev_map[m] & set(E_K)
        xt, xp, xc = signatures[m]
        bytes = 0
        for c in channels:
            n_tokens = signatures[c][0]
            n_size = signatures[c][1]
            bytes += n_tokens * n_size
        bytes = float(bytes)
        # the performance of a memory depends on the the number of
        # bytes sent to the memory, and the read/write rates
        r_read = xt[0]
        r_write = xt[1]
        performances[m] = bytes / r_read + bytes / r_write
        power_usage[m] = performances[m] * xp[0]
        cost[m] = xc[0] * bool(channels)
    return max(performances.itervalues()), sum(power_usage.itervalues()), sum(cost.itervalues())


if __name__ == "__main__":
    import sys
    from pysesame import sesame
    from sesamemap import apparchmap, appyml, archyml
    if len(sys.argv) != 2:
        print "usage: %s <simulation.yml>" % sys.argv[0]
        sys.exit(2)
    sp = sesame.SesameProject(sys.argv[1])
    app_yml_file, arch_yml_file, map_yml_file = sp.parse_simulation_yml()
    app = appyml.Application(app_yml_file)
    arch = archyml.Architecture(arch_yml_file)
    for i, m in enumerate(apparchmap.IterMappings(app, arch)):
        t, p, c = calc_objectives(m)
        print i, (t, p, c)
