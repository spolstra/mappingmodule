from pyyml import yml
from .misc import helperfunctions
import sys
import logging
import time
import networkx as NX

# make graph drawing optional
try:
    import pydot
    pydot_available = True
except ImportError:
    pydot_available = False

# setup logger
logger = logging.getLogger(__name__.split('.')[-1])
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class ArchitectureYMLLookup(yml.DefaultYMLLookup):

    def property_ancestor(self, element):
        for el in self.ancestors(element):
            if el.tag == '{%s}property' % yml.NAMESPACE:
                return True
        return False

    # Needed to easily access class property value in lookup
    def get_property_value(self, element, name):
        for p in element.getchildren():
            if p.tag == '{%s}property' % yml.NAMESPACE and \
                    p.get('name') == name:
                return p.get('value')

    # FIXME: changed template into defaulttemplate...
    # FIXME: 'class' property should be used to determine yml class.
    # Currently lookup checks 'vproc' or 'vproc_sched' (ticket #82)
    def lookup(self, document, element):
        templates = [t.get('value')
                     for t in self.properties(element, 'template')]
        if not self.property_ancestor(element):
            if templates:
                if 'vproc' in templates or 'vproc_sched' in templates:
                    return Processor
                else:
                    return Memory
            if (element.tag == '{%s}node' % yml.NAMESPACE and
                    not self.property_ancestor(element)):
                if self.get_property_value(element, 'class') == 'cbus':
                    return Interconnect
                elif self.get_property_value(element, 'class') == 'processor':
                    return Processor
                return Component
            elif (element.tag == '{%s}link' % yml.NAMESPACE and
                  not self.property_ancestor(element)):
                return Connection
            elif (element.tag == '{%s}network' % yml.NAMESPACE and
                  not self.property_ancestor(element)):
                if self.get_property_value(element, 'type') == 'crossbar':
                    return Interconnect
                if self.get_property_value(element, 'class') == 'dfifo':
                    return Interconnect
        return yml.DefaultYMLLookup.lookup(self, document, element)


class Component(yml.YMLNetwork):

    def __init__(self, **args):
        yml.YMLEntity.__init__(self, nsmap=yml.NSMAP)
        self.set('class', 'pearl_object')
        self.tag = '{%s}node' % yml.NAMESPACE
        for k, v in args.iteritems():
            self.set(k, v)

    def get_templates(self):
        return dict([(p.value, p) for p in self.get_property('template')])

    def get_template_mappings(self):
        return dict([(p.value, p) for p in self.get_property('mapping')])

    def get_component_class(self):
        c = self.xpath("ns:property[@name='class']",
                       namespaces={'ns': yml.NAMESPACE})
        if len(c) == 1:
            return c[0].get_value()
        else:
            raise yml.YMLError("Component '%s' should have a class "
                               "property. But it has %d" % (self.name, len(c)))

    def __lt__(self, o):
        return self.name < o.name

    def __repr__(self):
        return self.name

    def __str__(self):
        return "Component %s" % self.name

    def get_memory_size(self):
        return self.get_prop_val_safe('memory_size')


class Processor(Component):

    def __init__(self, **args):
        yml.YMLEntity.__init__(self, nsmap=yml.NSMAP)
        self.tag = '{%s}node' % yml.NAMESPACE
        # Processor specific attributes and properties
        self.set('class', 'pearl_object')
        self.add_property('class', 'processor')
        for k, v in args.iteritems():
            self.set(k, v)

    def get_allowed_operations(self):
        try:
            return set(eval(self.get_property('operations')[0].value))
        except Exception:
            return set()

    def get_type(self):
        type_p = self.get_property('type')
        if (type_p):
            return type_p[0].value
        else:
            return None

    def get_init_vector(self):
        init_vec = self.get_property('init')
        if (init_vec):
            return init_vec[0].value
        else:
            return None

    def set_init_vector(self, value):
        self.set_property('init', value)

    allowed_operations = property(get_allowed_operations)

    def __str__(self):
        return 'Processor %s' % self.name


class Memory(Component):

    def __str__(self):
        return 'Memory %s' % self.name


class Interconnect(Component):

    def __str__(self):
        return "Interconnect %s" % self.name


class Connection(yml.YMLLink):

    def __repr__(self):
        return yml.YMLLink.__str__(self)


class Architecture(list):

    def __init__(self, filename, dataflow=True, max_length=sys.maxsize):
        self.max_length = max_length
        self.yml = yml.parse(filename, ArchitectureYMLLookup)
        self.name = self.yml.name
        self.filename = filename

        # remove processors with $ in their name
        self.processors = sorted(
            n for n in self.yml.xpath("//ns:node|//ns:network",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(n, Processor) and not "$" in n.name)

        logger.debug("Architecture processors: %s" % self.processors)
        self.memories = sorted(
            n for n in self.yml.xpath("//ns:node|//ns:network",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(n, Memory))
        logger.debug("Architecture memories: %s" % self.memories)
        self.interconnects = sorted(
            n for n in self.yml.xpath("//ns:node|//ns:network",
                                      namespaces={'ns': yml.NAMESPACE})
            if isinstance(n, Interconnect))
        logger.debug("Architecture interconnects: %s" % self.interconnects)
        self.components = self.processors + self.memories + \
            self.interconnects
        self.components_index = dict((c, i)
                                     for i, c in enumerate(self.components))
        self.connections = sorted(l for l in self.yml.links())

        # A dict of components is useful for ymlprocessors
        self.component_table = dict([n.name, n] for n in self.components)

        # current yml does not have componentclass attrs.
        self.memory_components = set(self.memories)
        self.processor_components = set(self.processors)
        list.__init__(self, self.components + self.connections)

        self.dgraph = None
        logger.info("Architecture dataflow: %s" % dataflow)
        if dataflow:
            self.dgraph = DataFlowGraph(self.yml)
            self.__reachable_memories = self.precalc_reachable_memories()
        else:
            # If we don't do DFA we assume all memories are reachable
            self.__reachable_memories = {}
            for p1, p2 in ((i, j) for i in self.processors \
                    for j in self.processors):
                self.__reachable_memories[(p1, p2)] = self.memories


    def table(self):
        header = ('Index', 'Architecture component')
        data = [header] + [(i, c) for i, c in enumerate(self.components)]
        return helperfunctions.columns(data)

    def __str__(self):
        return "Architecture %s" % (self.name)

    def reachable_memories(self, p1, p2):
        return self.__reachable_memories[(p1, p2)]

    # Return list of component names connected to name
    def neighbors(self, name):
        neighborlist = []
        for link in self.connections:
            innode, outnode = link.get_nodes()
            if innode.name == name:
                neighborlist.append(outnode.name)
        return neighborlist

    def connected_components(self, comp):
        neighbor_list = self.neighbors(comp.name)
        parent = comp.getparentnetwork()
        neighbor_comps = [parent.get_node_or_network(name)[0]
                          for name in neighbor_list]
        return neighbor_comps

    def find_reachable_interconnects(self, processor):
        assert self.dgraph is not None

        for candidate_iconnect in self.interconnects:
            for pp, ip in ((pp, ip) for pp in processor.ports()
                           for ip in
                           candidate_iconnect.ports()):
                try:
                    path = NX.shortest_path(self.dgraph, pp, ip)
                    path_debug = [n.getparent() for n in path]  # debug
                    logger.debug("path_t:" + str(path_debug))
                except Exception:
                    pass  # ok to fail here

    def get_scheduler_processor(self, scheduler):
        comps = self.connected_components(scheduler)
        proc = filter(lambda c: c.get_component_class() == 'processor',
                      comps)
        if len(proc) != 1:
            raise yml.YMLError(
                "hwscheduler '%s' not connected to a processor" %
                scheduler)
        logger.debug("%s connected to %s" % (scheduler, proc[0]))
        return proc[0]

    def find_reachable_memories(self, src_processor, dest_processor):
        assert self.dgraph is not None

        # If source_processor or dest_processor is a
        # scheduler, skip to it's processor.
        src_class = src_processor.get_component_class()
        dest_class = dest_processor.get_component_class()
        logger.debug("source_proc: %s has class: %s" % (src_processor,
                                                        src_class))
        logger.debug("dest_proc: %s has class: %s" % (dest_processor,
                                                      dest_class))

        # If src is a hwscheduler replace it with it's processor
        if src_class == 'hwscheduler':
            src_processor = self.get_scheduler_processor(src_processor)

        # If dest is a hwscheduler replace it with it's processor
        if dest_class == 'hwscheduler':
            dest_processor = self.get_scheduler_processor(dest_processor)

        reachable_memories = set()
        proc = self.processor_components
        mems = self.memory_components
        for candidate_memory in self.memory_components:
            # find shortest path between each port-pair of the source processor
            # and candidate memory ports.
            for pp1, pm1 in ((pp1, pm1)for pp1 in src_processor.ports() for pm1 in candidate_memory.ports()):
                try:
                    path = NX.shortest_path(self.dgraph, pp1, pm1)
                    l1 = len(path)
                    path = set(n.getparent() for n in path[1:-1])
                    # if this is a valid path, i.e., not path through other
                    # processors and memory components, find a path from the
                    # the candidate memory to destination processor.
                    if not ((path & proc) or (path & mems)):
                        for pp2, pm2 in ((pp2, pm2) for pp2 in dest_processor.ports() for pm2 in candidate_memory.ports()):
                            path = NX.shortest_path(self.dgraph, pm2, pp2)
                            l2 = len(path)
                            path = set(n.getparent() for n in path[1:-1])
                            # valid path? -> add candidate memory to the
                            # set of reachable memories.
                            if (not (path & proc)) and \
                                    (l1 + l2 < self.max_length):
                                reachable_memories.add(candidate_memory)
                except NX.NetworkXNoPath:
                    pass  # no path, no problem
        # each reachable memory component must be an instance of Memory
        for m in reachable_memories:
            while not isinstance(m, Memory):
                # if m is not Memory, there must be an ancestor that is a
                # Memory, otherwise the architecture is incorrect
                reachable_memories.remove(m)
                m = m.getparent()
                if m:
                    reachable_memories.add(m)
                else:
                    raise yml.YMLError(
                        "memory component without Memory parent")
        return reachable_memories

    def precalc_reachable_memories(self):
        # calculate reachable memory set for each pair of processors
        reachable_memories = {}
        logger.debug("Precalc reachable memories")
        for p1, p2 in ((i, j) for i in self.processors for j in self.processors):
            reachable_memories[(p1, p2)] = self.find_reachable_memories(p1, p2)

            logger.debug("Reachable memories "
                         "for %s, %s: %s" %
                         (p1, p2, reachable_memories[(p1, p2)]))
        return reachable_memories

    def write(self, filename, pretty=True):
        tree = self.yml.getroottree()
        tree.write(filename, pretty_print=pretty)


class DataFlowGraph(yml.YMLGraph):

    def __init__(self, arch_yml, data=None, name=None, **kwds):
        yml.YMLGraph.__init__(self, arch_yml, data=data, name=None, **kwds)
        self.name = "%s dataflow graph" % self.yml.name
        self.add_nodes_from(self.xpath('port'))
        self.add_edges_from(self.get_connections())

    def xpath(self, element):
        return self.yml.xpath(
            '//ns:%s[not(ancestor::*[self::ns:property])]' %
            element, namespaces={'ns': yml.NAMESPACE})

    def get_connections(self):
        # find all architecture components
        components = self.xpath('node')
        logger.debug("Components used in get_connections %s" % components)

        # find all connections between ports
        external_connections = set(c.ports for c in self.xpath('link'))
        connections = external_connections
        # it is assumed that within an architecture component, all ports are
        # fully connected, therefore a fully connected subgraph is built
        # between the ports that belong to the same architecture component
        for component in components:
            # all ports belonging to this component
            ports = [port for port in component.ports()]
            # calculate intra-component connections
            internal_connections = set(self.full_connect(ports))
            # add intra-component connections to set of all connections
            connections |= internal_connections
        return connections

    def make_pydot(G):
        if not pydot_available:
            return None
        graph = pydot.Dot(graph_type='digraph')
        for v in G.nodes_iter():
            v = v.print_path()
            graph.add_node(pydot.Node(str(v)))
        added_edges = set()
        for e in G.edges_iter():
            (u, v) = e
            u = u.print_path()
            v = v.print_path()
            if u != v and (u, v) not in added_edges:
                added_edges.add((u, v))
                graph.add_edge(pydot.Edge(str(u), str(v)))
        return graph


if __name__ == "__main__":
    import sys

    # We want to see all messages for this test.
    logger.setLevel(logging.DEBUG)

    if len(sys.argv) != 2:
        print "usage: %s <arch_yml-file>" % sys.argv[0]
        sys.exit(2)

    logger.debug("Reading architecture...")
    t_start = time.time()
    parsed_arch = yml.parse(sys.argv[1], ArchitectureYMLLookup)
    # print lxml.objectify.dump(parsed_arch)
    t_total = time.time() - t_start
    logger.debug('Parsing architecture yml took: %f s' % t_total)

    # Time Architecture Construction with and without Data Flow Analysis.
    for use_dfa in [False, True]:
        t_start = time.time()
        arch = Architecture(sys.argv[1], use_dfa, 25)
        t_total = time.time() - t_start
        logger.debug('Constructing Architecture %s took: %f s' %
                     ("with DFA" if use_dfa else "without DFA", t_total))

    print arch
    print arch.table()
    print arch.memory_components
    print arch.processor_components

    for c, s in arch.precalc_reachable_memories().iteritems():
        print c, s
    dot_graph = arch.dgraph.make_pydot()
    if dot_graph is not None:
        dot_graph.write_dot('dgraph.dot')
    else:
        print "pydot is not installed"

    print "Memory-like components that are channel destinations:"
    print arch.memory_components
    for c in arch.memory_components:
        print c
        print type(c)
