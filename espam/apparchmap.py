from . import espamapp
from . import espamplatform
from . import espammap
import random
import logging
from sesamemap.misc import helperfunctions

# Espam version of apparchmap
# NOTE: Espam only describes the task mapping, not the channel mapping.

# Simpler than the YML version, it will provide:
# - dict interface to the mapping.
# - randomize: generate a random mapping.
# - all_mapped(): set of arch components used.
# - export_xml: Generate mapping xml from the apparchmapping.

# - Feasible mapping? Do we have enough information to implement this?
#   Without it we cannot repair mappings.


class AppArchMap(dict):

    def __init__(self, app, arch, *args, **kwargs):
        self.app = app
        self.arch = arch
        dict.__init__(self, *args, **kwargs)

        if set(self.app.processes + self.app.channels) != set(self.iterkeys()):
            # If our AppArchMap was not already initialized via args
            # or kwargs, initialise it with a random mapping.
            self.randomize()

    def reverse_map(self):
        revmap = dict((v, set()) for v in self.arch.processors)
        for app_node, arch_node in self.iteritems():
            revmap[arch_node].add(app_node)
        return revmap

    def feasible_mappings(self, app_node):
        # Don't know how to determine the feasible mappings for an
        # espam project. For now _all_ processor mappings are feasible.
        return self.arch.processors

    def __setitem__(self, app_node, arch_node):
        if arch_node in self.feasible_mappings(app_node):
            dict.__setitem__(self, app_node, arch_node)
        else:
            raise Exception("not allowed: %s->%s" % (app_node, arch_node))

    def is_mapped(self, app_node, arch_node):
        return self[app_node] == arch_node

    def is_valid(self):
        for process in self.app.processes:
            if self[process] not in self.feasible_mappings(process):
                return False
        return True

    def repair(self):
        # Tasks should be mapped onto processors only.
        for process in self.app.processes:
            feasible_set = self.feasible_mappings(process)
            if not self[process] in feasible_set:
                # Randomly pick a processor for this task
                self[process] = random.choice(sorted(feasible_set))

    def randomize(self):
        """Generate a random, valid Application->Architecture mapping."""
        for process in self.app.processes + self.app.channels:
            self[process] = random.choice(
                sorted(self.feasible_mappings(process)))

    def all_mapped(self):
        """Return the set of architecture components used in this mapping"""
        return sorted(set(self.values()))

    def __str__(self):
        header = ('Kahn process', 'Architecture component')
        data = []
        for app_node, arch_node in self.iteritems():
            data.append((str(app_node), str(arch_node)))
        data.sort()
        data = [header] + data
        return helperfunctions.columns(data, "%%-%ds ==> %%-%ds")

    # FIXME: Until we have a common interface for yml and espam, do this.
    def export_yml(self):
        return self.export_xml()

    def export_xml(self):
        # Reverse because espam mappings are written as proc->tasks
        # And our map is task->proc,..
        revmap = self.reverse_map()
        xml = espammap.Mapping(name='myMapping')
        for proc, tasks in revmap.iteritems():
            if tasks:
                procmap = espammap.Processor(name=proc.name)
                for t in tasks:
                    procmap.append(espammap.Process(name=t.name))
                xml.append(procmap)
        return xml


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 3:
        print "usage: apparchmap.py <app> <arch>"
        exit(2)

    app = espamapp.Application(sys.argv[1])
    arch = espamplatform.EspamPlatform(sys.argv[2])
    print "processors: %s" % arch.processors
    m = AppArchMap(app, arch)

    print "apparchmap: %s" % m
    print "writing random mapping to file test.map"
    xmlmap = m.export_xml()
    espammap.to_file(xmlmap, "test.map")

    # Construct AppArchMap with given mapping.
    # 'd' maps all tasks onto the first component of the architecture.
    d = [(task, arch.components[0]) for task in app.processes + app.channels]
    m2 = AppArchMap(app, arch, d)
    print "apparchmap: %s" % m2
    xmlmap = m2.export_xml()
    espammap.to_file(xmlmap, "test2.map")
