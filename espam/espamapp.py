import lxml.objectify
import lxml.etree
import lxml.pyclasslookup
import os.path
import networkx as NX
from .espamplatform import EspamEntity
from sesamemap.misc import helperfunctions

# Parser for espam application files (*.kpn)

# An application description can contain (sobel taken as example):
# * sadg
# * adg
# * parameter

# * node
# * outport
# * bindvariable
# * domain
# * linearbound
# * constraint
# * context

# * edge
# * linearization
# * mapping

# * ast
# * for
# * stmt
# * if
# * port


class Node(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'node'

    # Comparision function is important, we want deterministic
    # ordering. The task identifiers are assigned by ordering the
    # task names alphabetically.
    def __cmp__(self, o):
        return cmp(self.name, o.name)


class Edge(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'edge'

    def from_node(self):
        return self.get('fromNode')

    def to_node(self):
        return self.get('toNode')

    def get_buffer_size(self):
        return self.get('size')


class Token(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'bindvariable'

    def data_type(self):
        return self.get('dataType')


class OutPort(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'outport'

    def get_edge(self):
        return self.get('edge')


class InPort(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'inport'

    def get_edge(self):
        return self.get('edge')


class EspamAppLookup(lxml.pyclasslookup.PythonElementClassLookup):

    def lookup(self, document, element):
        if element.tag == 'node':
            return Node
        if element.tag == 'edge':
            return Edge
        else:
            return EspamEntity


def parse(filename, class_lookup=EspamAppLookup):
    parser = lxml.etree.XMLParser(remove_blank_text=True,
                                  remove_comments=True)
    parser.setElementClassLookup(class_lookup())
    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()
    return tree.getroot()


class Application:

    def __init__(self, filename, dataflow=True):
        self.xml = parse(filename)
        self.processes = sorted(n for n in self.xml.xpath("//node"))
        self.channels = sorted(l for l in self.xml.xpath("//edge"))
        self.kpn_elements = self.processes + self.channels
        # Mapped_elements are used in design space exploration.
        self.mapped_elements = self.processes

        if dataflow:
            self.dgraph = DataFlowGraph(self)
        else:
            self.dgraph = None

    def write(self, filename):
        # write(tostring(..)) used here because
        # lxml.etree.write(..) does  not allow us to specify
        # the doctype.
        f = open(filename, 'w')
        f.write(lxml.etree.tostring(self.xml,
                                    pretty_print=True, standalone=False,
                                    encoding=None,
                                    doctype='<!DOCTYPE platform PUBLIC "-//LIACS//DTD ESPAM 1//EN" "http://www.liacs.nl/~cserc/dtd/espam_1.dtd">'))
        f.close()

    def table(self):
        header = ('Index', 'Kahn process/channel name')
        data = [header] + list(enumerate(self.processes +
                                         self.channels))
        return helperfunctions.columns(data)

    def out_channels(self, process):
        out_ch = []
        for c in self.channels:
            if c.get("fromNode") == process:
                out_ch.append(c)
        return out_ch

    def cns_processes(self, process):
        cns_p = []
        for c in self.channels:
            if c.get("fromNode") == process:
                cns_p.append(c.get("toNode"))
        return cns_p

    def in_channels(self, process):
        in_ch = []
        for c in self.channels:
            if c.get("toNode") == process:
                in_ch.append(c)
        return in_ch

    def prd_processes(self, process):
        prd_p = []
        for c in self.channels:
            if c.get("toNode") == process:
                prd_p.append(c.get("fromNode"))
        return prd_p

    def get_token_type(self, channel):
        for channels in self.xml.findall(".//outport"):
            if channels.get('edge') == channel:
                token_type = channels.find('bindvariable').get('dataType')
                return token_type

#    def get_token_size(token_type):
#        t_size = types.token_size.get(token_type)
#        return t_size

#    def get_token_size(self, channel):
#        for channels in self.xml.findall(".//outport"):
#            if channels.get('edge') == channel:
#                token_type = channels.find('bindvariable').get('dataType')
#                t_size = token_size.get(str(token_type))
#        return t_size


class DataFlowGraph(NX.DiGraph):

    def __init__(self, app):
        self.app = app
        NX.DiGraph.__init__(self, data=None)
        self.add_nodes_from(n.name for n in app.processes)
        self.add_edges_from((c.from_node(), c.to_node()) for
                            c in app.channels)

# Unit tests.
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <espam application file>" % sys.argv[0]
        sys.exit(2)

    # Parse Espam application description.
    app = Application(sys.argv[1])

    # Print table containing the tasks and channels of the application.
    print app.table()
    print

    # Print the application channels with source and destination tasks.
    print "Link:  Components"
    for c in app.channels:
        print "%5s: %s -> %s" % (c, c.get("fromNode"),
                                 c.get("toNode"))

    # Write the application in dot graph format.
    NX.write_dot(app.dgraph, "application.dot")

    # Some tests - jelena
    print "Output channels per process"
    for p in app.processes:
        print "%5s: %s" % (p, app.out_channels(str(p)))

    print "Consumption processes per process"
    for p in app.processes:
        print "%5s: %s" % (p, app.cns_processes(str(p)))

    print "Input channels per process"
    for p in app.processes:
        print "%5s: %s" % (p, app.in_channels(str(p)))

    print "Production processes per process"
    for p in app.processes:
        print "%5s: %s" % (p, app.prd_processes(str(p)))

    print "Buffer size per channel"
    for c in app.channels:
        print "%5s: %s" % (c, c.get_buffer_size())

    print "Token type per channel"
    for channel in app.xml.findall(".//outport"):
        print "%5s: %s" % (channel.get('edge'),
                           channel.find('bindvariable').get('dataType'))

#    print "Token size per channel"
#    for channel in app.xml.findall(".//outport"):
# print "%5s: %s" % (channel.get('edge'),
# app.get_token_size(channel.get('edge')))

    print "Domain matrix of a channel"
    for channel in app.xml.findall(".//outport"):
        print "%5s: %s" % (channel.get('edge'),
                           channel.find('domain/linearbound/constraint').get('matrix'))

    print "Domain matrix of a process"
    for p in app.processes:
        print "%5s: %s" % (p,
                           p.find('function/domain/linearbound/constraint').get('matrix'))
