import lxml.objectify
import lxml.etree
import lxml.pyclasslookup
import os.path
import networkx as NX

# Parser for espam platform files.

# A platform description can contain:
# * processor
# * platform
# * network
# * link
# * port
# * resource
# * host_interface


class EspamEntity(lxml.objectify.ObjectifiedElement):

    def __init__(self, **args):
        super(EspamEntity, self).__init__()
        for k, v in args.iteritems():
            self.set(k, v)

    def get_name(self):
        return self.get('name')

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    name = property(get_name)


class Processor(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'processor'

    def get_type(self):
        return self.get('type')


class Platform(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'platform'


class Network(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'network'


class Link(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'link'

    def get_components(self):
        comps = self.xpath("resource")
        return comps[0], comps[1]

    components = property(get_components)


class Port(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'port'


class Host_interface(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'host_interface'

    def get_type(self):
        return self.get('type')


class Resource(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self, **args)
        self.tag = 'resource'


class EspamPlatformLookup(lxml.pyclasslookup.PythonElementClassLookup):

    def lookup(self, document, element):
        if element.tag == 'processor':
            return Processor
        if element.tag == 'platform':
            return Platform
        if element.tag == 'network':
            return Network
        if element.tag == 'link':
            return Link
        if element.tag == 'port':
            return Port
        if element.tag == 'host_interface':
            return Host_interface
        if element.tag == 'resource':
            return Resource
        else:
            return EspamEntity


def parse(filename, class_lookup=EspamPlatformLookup):
    parser = lxml.etree.XMLParser(remove_blank_text=True,
                                  remove_comments=True)
    parser.setElementClassLookup(class_lookup())

    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()

    return tree.getroot()


class EspamPlatform(list):

    def __init__(self, filename, dataflow=True):
        self.xml = parse(filename)
        # self.name = self.xml.name
        self.processors = [n for n in
                           self.xml.xpath("//processor")]  # before was sorted, but when bellow line is added, sorted does not work - jelena
#        self.processor_types = [n.get_type() for n in
# self.xml.xpath("//processor")] # jelena
        self.links = sorted(n for n in self.xml.xpath("//link"))
        self.networks = sorted(n for n in
                               self.xml.xpath("//network"))
        self.host_interfaces = sorted(n for n in
                                      self.xml.xpath("//host_interface"))
        self.components = self.processors + self.networks \
            + self.host_interfaces
        self.components_index = dict((c, i)
                                     for i, c in enumerate(self.components))
        list.__init__(self.components + self.links)

        self.dgraph = None
        if dataflow:
            self.dgraph = DataFlowGraph(self)


def to_file(platform, filename):
    # write(tostring(..)) used here because lxml.etree.write(..) does
    # not allow us to specify the doctype.
    f = open(filename, 'w')
    f.write(lxml.etree.tostring(platform,
                                pretty_print=True, standalone=False,
                                encoding=None,
                                doctype='<!DOCTYPE platform PUBLIC "-//LIACS//DTD ESPAM 1//EN" "http://www.liacs.nl/~cserc/dtd/espam_1.dtd">'))
    f.close()


class DataFlowGraph(NX.DiGraph):

    def __init__(self, platform):
        self.platform = platform
        NX.DiGraph.__init__(self, data=None)
        self.add_nodes_from((n.name for n in platform.components))
        self.add_edges_from(
            map(lambda c1_c2: (c1_c2[0].name, c1_c2[1].name),
                (l.get_components() for l in platform.links)))
        # debug
        # print "Nodes: " + str(self.nodes())
        # print "Edges: " + str(self.edges())


# Unit tests.
if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print "usage: %s <espam-xml-file>" % sys.argv[0]
        sys.exit(2)
    # Parse Espam platform description.
    platform = EspamPlatform(sys.argv[1])
    # Print components of the platform by type.
    print "Processors: %s" % platform.processors
# print "Processor types: %s" % platform.processor_types #jelena
    processor_types = []
    for p in platform.processors:
        processor_types.append(p.get_type())
    print "Processor types: %s" % processor_types  # jelena

    print "Links:      %s" % platform.links
    print "Networks:   %s" % platform.networks
    print "Interfaces: %s" % platform.host_interfaces
    host_if_types = []
    for h in platform.host_interfaces:
        host_if_types.append(h.get_type())
    print "Interface types: %s" % host_if_types

    print

    # Test writing the platform description to file.
    to_file(platform.xml, "t.pla")

    llinks = sorted(l.get_components() for l in platform.links)
    print "Links connections: %s" % llinks
    print

    print "Name: Components"
    print "----------------"
    for l in platform.links:
        print "%s: %s" % (l.name, l.get_components())

    NX.write_dot(platform.dgraph, "platform.dot")

    exit(0)
    root = platform.xml

    print lxml.objectify.dump(root)

    print "new object"
    new = Platform(name="myplatform")
    new.append(Processor(name="mine"))
    print lxml.objectify.dump(new)
