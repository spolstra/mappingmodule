import os
import logging
from . import espammap

from lxml.etree import parse

# There is no 'official' definition for an espam project (yet).
# I assume its similar to the yml version:
# <simulation>
#  <application src="simple.kpn"/>
#  <mapping src="simple.map"/>
#  <architecture src="simple.pla"/>
# </simulation>


class EspamProject:

    def __init__(self, simulation_xml_path):
        self.simulation_xml = os.path.abspath(simulation_xml_path)
        self.path = os.path.dirname(self.simulation_xml)
        self.name = os.path.split(self.path)[-1]

    def parse_simulation_xml(self):
        simulation_xml = parse(self.simulation_xml).getroot()
        app_xml_file = simulation_xml.find('application').get('src')
        arch_xml_file = simulation_xml.find('architecture').get('src')
        map_xml_file = simulation_xml.find('mapping').get('src')
        # Store names (update_ymlmap uses them)
        self.app_filename = os.path.join(self.path, app_xml_file)
        self.arch_filename = os.path.join(self.path, arch_xml_file)
        self.map_filename = os.path.join(self.path, map_xml_file)
        return self.app_filename, self.arch_filename, self.map_filename

    # FIXME: We should refactor all these calls and use a uniform
    # interface for both yml and espam. Call it update_xmlmap
    def update_ymlmap(self, m, map_filename=None):
        self.update_xmlmap(m, map_filename)

    def update_xmlmap(self, m, map_filename=None):
        # Get the mapfilename to write to.
        if map_filename is None:
            # Use project mapfilename
            if self.map_filename is None:
                # If filenames are not set, parse the project file first
                self.parse_simulation_xml()
            map_filename = self.map_filename
        map_xml = m.export_xml()
        espammap.to_file(map_xml, map_filename)

    def runarch(self, app, arch, mapping, verbose=False):
        logging.info("Dummy espam runarch method called.")
        # calculate temporary power estimate. This is a placeholder
        # for Jelena's code.
        p = self.estimate_power(app, arch, mapping)
        return p

    def clean(self):
        logging.info("Dummy espam clean method called.")

    def build_all(self):
        logging.info("Dummy espam build_all method called.")

    def trace(self):
        logging.info("Dummy espam trace method called.")

    def __repr__(self):
        return "EspamProject %s: %s" % (self.name, self.simulation_xml)

    def __str__(self):
        return str((self.path, self.name))

    def estimate_power(self, app, arch, mapping):
        # Need some placeholder function here
        # Let's assume that spreading the tasks evenly is good for
        # power consumption.
        # average = n_tasks / n_proc
        # cost = sum( abs(average-load(p)), p=0..n_proc)
        average = len(app.mapped_elements) / len(arch.processors)
        logging.debug("number of procs: %d" % len(app.mapped_elements))
        logging.debug("number of tasks: %d" % len(arch.processors))
        logging.debug("average: %d" % average)

        cost = 0
        revmap = mapping.reverse_map()
        for p, tasks in revmap.iteritems():
            cost += abs(average - len(tasks))
        return cost


if __name__ == "__main__":
    import sys
    s = EspamProject(sys.argv[1])
    print s
    print repr(s)
    print s.parse_simulation_xml()
