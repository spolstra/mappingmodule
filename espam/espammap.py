import lxml.objectify
import lxml.etree
import os.path
from collections import defaultdict
import pyyml.ymlmap
import collections
from .espamplatform import EspamEntity
from . import espamapp
from . import espamplatform

# Parser (and yml converter) for espammap files.

# A mapping can contain the elements:
# * mapping
# * processor
# * process


class Mapping(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self)
        self.tag = 'mapping'
        for k, v in args.iteritems():
            self.set(k, v)

    def __repr__(self):
        return self.name


class Processor(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self)
        self.tag = 'processor'
        for k, v in args.iteritems():
            self.set(k, v)

    def __repr__(self):
        return self.name


class Process(EspamEntity):

    def __init__(self, **args):
        EspamEntity.__init__(self)
        self.tag = 'process'
        for k, v in args.iteritems():
            self.set(k, v)

    def __repr__(self):
        return self.name


class EspamMapLookup(lxml.pyclasslookup.PythonElementClassLookup):

    def lookup(self, document, element):
        if element.tag == 'processor':
            return Processor
        if element.tag == 'process':
            return Process
        if element.tag == 'mapping':
            return Mapping
        else:
            return EspamEntity


def parse(filename, class_lookup=EspamMapLookup, validate=True):
    parser = lxml.etree.XMLParser(remove_blank_text=True,
                                  remove_comments=True)
    parser.setElementClassLookup(class_lookup())

    xml = open(filename)
    tree = lxml.etree.parse(xml, parser)
    xml.close()

    return tree.getroot()


# returns node-processor mapping list
# - e.g. [(ND_0, mb_1), (ND_1, mb_1), (ND_2, mb_2), (ND_3, mb_2),
# (ND_4, mb_3)]
def node_proc(xml):
    node_proc_list = []
    for n in xml.xpath("//process"):
        node_proc_list.append((n, n.getparent()))
    return node_proc_list


# returns mapping dictionary
# - e.g. {'mb_1': [ND_0, ND_1], 'mb_2': [ND_2, ND_3], 'mb_3': [ND_4]}
def nodes_proc(xml):
    nodes2proc = defaultdict(list)
    for i in xml.findall(".//processor"):
        for j in i.findall("process"):
            nodes2proc[str(i)].append(j)
    return nodes2proc


# returns mapping list, where processor is index of an element
# - e.g [[ND_0, ND_1], [ND_2, ND_3], [ND_4]]
# (SP) This assumes that the processors are ordered in the xml file.
#      Is that safe?
def nodes_map(xml):
    nodes_mapping = list()
    for i in xml.findall(".//processor"):
        proc_nodes = [j for j in i.findall("process")]
        nodes_mapping.append(proc_nodes)
    return nodes_mapping


# generates:
# 1) read contention matrix C for a mapping where Cij = 1, if there is
#    a channel from processor i to processor j, Cij = 0 otherwise
# 2) read channels matrix R for a mapping where Rij contains ID of
#    channels from processor i to processor j if Cij = 1 (e.g. [ED_0,
#    ED_1]), or 0 if Cij = 0
def gen_contention_matrix(platform, nodes_map, application):
    no_of_processors = len(platform.processors)
    contention = [[0 for i in range(no_of_processors)]
                  for i in range(no_of_processors)]
    r_ch = [[0 for i in range(no_of_processors)]
            for i in range(no_of_processors)]
    for i in range(no_of_processors):
        for j in nodes_map[i]:
            prd_processes = application.prd_processes(str(j))
            for k in range(len(prd_processes)):
                if len(prd_processes) != 0:
                    for r in range(no_of_processors):
                        for q in nodes_map[r]:
                            if str(q) == prd_processes[k]:
                                contention[r][i] = 1
                                if r_ch[r][i] == 0:
                                    r_ch[r][i] = [
                                        application.in_channels(str(j))[k]]
                                else:
                                    r_ch[r][i].append(
                                        application.in_channels(str(j))[k])
    return contention, r_ch


# returns ID of all read channels of a processor
def get_processor_read_channels(processor, r_ch):
    channels = list()
    for i in range(len(r_ch[0])):
        if r_ch[i][processor] != 0:
            for j in range(len(r_ch[i][processor])):
                channels.append(r_ch[i][processor][j])
    return channels


# returns total time in clock cycles (for whole application) which
# processor spends waiting for data due to reading the data through
# shared structure
def get_processor_contention(channels, ch_contention, channel_rep):
    p_contention = 0
    for i in range(len(channels)):
        p_contention += int(
            channel_rep[str(channels[i])]) * ch_contention[channels[i]]
    return p_contention


# returns ID of all write channels of a processor
def get_processor_write_channels(processor, nodes_map, application):
    channels = list()
    for i in range(len(nodes_map[processor])):
        if len(application.out_channels(str(nodes_map[processor][i]))) != 0:
            for j in application.out_channels(str(nodes_map[processor][i])):
                channels.append(j)
    return channels


def new_mapping(source='application', dest='architecture'):
    '''Make new mapping from a specific source to a specific architecture'''
    pass


# TODO compare with to_file() below
def store_mapping(filename, mapping):
    tree = mapping.getroottree()
    tree.write(filename, pretty_print=True,
               xml_declaration=True,
               encoding='UTF-8')


def to_espam(mapping):
    """Convert mapping to espam_1.dtd format.
    Only task mappings are written.
    All hierarchy in mapping is flattened.
    """
    # Create processor map.
    procMap = collections.defaultdict(set)
    # FIXME: will the defaulttmpl always be called 'vproc'. I don't
    # think we can depend on that.
    # We use it to detect processors, but we might need to read the
    # architecture to be sure.
    for child in mapping.findall(".//{%s}map[@dtmpl='vproc']" %
                              pyyml.ymlmap.NAMESPACE):
        procMap[child.dest].add(child.source)

    xmlmap = Mapping(name="myMapping")
    for procname in procMap:
        # Make processor element.
        proc = Processor(name=procname)
        xmlmap.append(proc)
        # Add all processes to this processor.
        for task in procMap[procname]:
            proc.append(Process(name=task))

    return xmlmap


def to_file(mapping, filename):
    # write(tostring(..)) used here because lxml.etree.write(..) does
    # not allow us to specify the doctype.
    f = open(filename, 'w')
    f.write(lxml.etree.tostring(mapping,
                                pretty_print=True,
                                standalone=False,
                                encoding=None,
                                doctype='<!DOCTYPE mapping PUBLIC "-//LIACS//DTD ESPAM 1//EN" "http://www.liacs.nl/~cserc/dtd/espam_1.dtd">'))
    f.close()

# Unit tests.
if __name__ == "__main__":
    import sys
    # Here we test both espam mapping parsing, and simple conversion.

    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print "usage: %s <espam-map-file> [<ymlmap-file>]" % sys.argv[0]
        sys.exit(2)
    elif len(sys.argv) == 3:
        # If we get a yml mapping file we also test a simple
        # conversion.
        root = pyyml.ymlmap.parse(sys.argv[2])

    # Read espam mapping from file.
    espamroot = parse(sys.argv[1])

    node_proc_list = node_proc(espamroot)
    nodes_proc_list = nodes_proc(espamroot)
    nodes_mapping = nodes_map(espamroot)
    print "node_proc_list: %s" % node_proc_list
    print "nodes_proc_list: %s" % nodes_proc_list
    print "nodes_mapping: %s" % nodes_mapping

    # write parsed espam mapping directly back to file:
    to_file(espamroot, "test-map.xml")

    # Show that we can simply generate a new YML mapping from scratch.
    r = pyyml.ymlmap.new_mapping('application', 'architecture')
    print lxml.objectify.dump(r)

    # Some more map creating here.
    print
    r1 = r.get_inner_mapping()
    r1.add_map("A", "procA", "mytemplate")
    r1.add_map("Control.Command2-&gt;VLE.Command", "memory", "vmem")
    print lxml.etree.tostring(r.getroottree(), pretty_print=True)

    # Do conversion if we got three arguments.
    if len(sys.argv) == 3:
        # convert yml mapping to espam mapping.
        espam_map = to_espam(root)
        # And write it to file.
        to_file(espam_map, "t.map")
        # Dump yml mapping (commented, generates a lot of ouput)
        # print lxml.objectify.dump(root)
        # Test writing (commented)
        # store_mapping("test_map.yml", root)
