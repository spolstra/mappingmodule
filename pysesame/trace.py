from __future__ import with_statement
import array
import os
import stat  # to determine trace file size

# each entry in the trace file consists of four signed shorts
ENTRY_TYPE = 'h'
ENTRY_SIZE = 4

T_QUIT = -1
T_NULL = 0
T_EXECUTE = 1
T_READ = 2
T_WRITE = 3

te_types = {
    T_QUIT: 'T_QUIT',
     T_NULL: 'T_NULL',
     T_EXECUTE: 'T_EXECUTE',
     T_READ: 'T_READ',
     T_WRITE: 'T_WRITE'}
max_te_type = max(len(x) for x in te_types.itervalues())
print_event_str = "%%-%ds  %%-%dd %%-%dd %%-%dd" % (max_te_type, 6, 6, 6)


class Trace:

    def __init__(self, filename):
        self.filename = filename
        file_size = os.stat(self.filename)[stat.ST_SIZE]
        self.trace_array = array.array(ENTRY_TYPE)
        with open(self.filename, 'rb') as trace_file:
            self.trace_array.fromfile(
                trace_file,
                file_size /
                self.trace_array.itemsize)
        self.trace_array.byteswap()
        self.filtered_trace = {}
        for et in te_types:
            self.filtered_trace[et] = [e for e in self.filterevent(et)]

    def tracefile(self):
        ENTRY_SIZE_LOCAL = ENTRY_SIZE
        trace_local = self.trace_array
        offset = 0
        max_offset = len(trace_local)
        while offset < max_offset:
            next_offset = offset + ENTRY_SIZE_LOCAL
            yield trace_local[offset:next_offset]
            offset = next_offset

    def filterevent(self, event):
        ENTRY_SIZE_LOCAL = ENTRY_SIZE
        trace_local = self.trace_array
        offset = 0
        max_offset = len(trace_local)
        while offset < max_offset:
            next_offset = offset + ENTRY_SIZE_LOCAL
            if trace_local[offset] == event:
                yield trace_local[offset + 1:next_offset]
            offset = next_offset

    def __repr__(self):
        return "tracefile %s" % (os.path.abspath(self.filename))

    def __str__(self):
        tracefile_handle = self.tracefile()
        return '\n'.join(print_event_str % (te_types[e[0]], e[1], e[2], e[3]) for e in tracefile_handle)

    def __len__(self):
        return len(self.trace_array) / ENTRY_SIZE


if __name__ == "__main__":
    import sys
    import time
    print "loading trace file"
    t_start = time.time()
    t = Trace(sys.argv[1])
    t_total = time.time() - t_start
    print repr(t)
    print t_total, len(t) / t_total
    print "parsing trace file"
    t_start = time.time()
    for e in t.tracefile():
        continue
    t_total = time.time() - t_start
    print t_total, len(t) / t_total
    print "filter trace file"
    for et in te_types:
        t_start = time.time()
        n_events = len(list(t.filterevent(et)))
        t_total = time.time() - t_start
        print te_types[et], n_events
        print t_total, n_events / t_total
