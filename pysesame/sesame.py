import os
import subprocess
import pyyml.ymlmap
import logging
from pyyml import yml
from lxml.etree import parse

# setup logger
logger = logging.getLogger(__name__.split('.')[-1])
# logger.setLevel(logging.DEBUG)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class SesameProject:

    def __init__(self, simulation_yml_path):
        self.simulation_yml = os.path.abspath(simulation_yml_path)
        if not os.path.exists(self.simulation_yml):
            raise yml.YMLError("Cannot open project\nProject file: {} "
                               "does not exist".format(self.simulation_yml))
        self.path = os.path.dirname(self.simulation_yml)
        self.name = os.path.split(self.path)[-1]
        # Read project yml file and set (app/arch/map)_yml_file
        self.app_yml_file, self.arch_yml_file, self.map_yml_file =\
            self.parse_simulation_yml()

    def parse_simulation_yml(self):
        # FIXME: parse_simulation is called in update yml mapping.
        # This will reparse the project file every time. We need to
        # store the filenames here.

        simulation_yml = parse(self.simulation_yml).getroot()
        app_yml_file = simulation_yml.find(
            '{%s}application' % yml.NAMESPACE).get('src')
        arch_yml_file = simulation_yml.find(
            '{%s}architecture' % yml.NAMESPACE).get('src')
        map_yml_file = simulation_yml.findall(
            '{%s}mapping' % yml.NAMESPACE)[-1].get('src')
        app_yml_file = os.path.join(self.path, app_yml_file)
        arch_yml_file = os.path.join(self.path, arch_yml_file)
        map_yml_file = os.path.join(self.path, map_yml_file)
        return app_yml_file, arch_yml_file, map_yml_file

    @property
    def map_file():
        """Return fully qualified mapping file name"""
        return self.map_yml_file

    def __print_output(self, sim_output):
        return '\n'.join(
                line for line in sim_output.splitlines()
                if line.startswith('app: ') or line.startswith('arch: '))

    def __run_make(self, target, directory=None):
        if not directory:
            directory = self.path
        return subprocess.Popen(
                'make %s' % target, cwd=directory, shell=True,
                bufsize=-1, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)

    def update_yml(self):
        assert self.__run_make('yml').wait() == 0

    def update_ymlmap(self, m, map_yml_filename=None):
        """ Update ymlmap file with mapping 'm'.
        If map_yml_filename is given use that, otherwise use the
        ymlmap filename of the project."""

        if map_yml_filename is None or not str(map_yml_filename).strip():
            map_yml_filename = self.map_yml_file
        logger.debug("yml map filename: %s" % map_yml_filename)
        map_yml = m.export_yml()
        pyyml.ymlmap.store_mapping(map_yml_filename, map_yml)
        # Cannot update_yml with a custom mapping file. Need to
        # create full mapping first.
        if map_yml_filename:
            return True
        return self.update_yml()

    def clean_yml(self):
        assert self.__run_make('yml-clean').wait() == 0

    def clean(self):
        assert self.__run_make('clean').wait() == 0

    def build(self):
        assert self.__run_make('build-arch').wait() == 0

    def build_all(self):
        assert self.__run_make('all').wait() == 0

    def runapparch(self, verbose=False):
        '''Run a cosimulation of the application and the architecture.

        By default, the simulation output is returned.
        If verbose=True, the simulation output is prefixed with
        app/arch tags.
        '''
        self.clean_yml()
        output, error = self.__run_make('run').communicate()
        if error:
            return error
        if verbose:
            return self.__print_output(output)
        return output

    def runapp(self):
        '''Run the application using PNRunner.
        '''
        self.clean_yml()
        output, error = self.__run_make('runapp').communicate()
        if error:
            return error
        return self.__print_output(output)

    def trace(self):
        '''Create Sesame trace files by running the application.
        '''
        self.clean_yml()
        self.update_yml()
        output, error = self.__run_make('trace').communicate()
        if error:
            return error
        return self.__print_output(output)

    def runarch(self, app=None, arch=None, mapping=None, verbose=False):
        '''Run the architecture using trace files from the application.

        By default, the simulation output is returned.
        If verbose=True, the simulation output is prefixed with app/arch.
        '''
        self.clean_yml()
        output, error = self.__run_make('runarch').communicate()
        if error:
            return error
        if verbose:
            print self.__print_output(output)
        return output

    def __repr__(self):
        return "SesameProject %s: %s" % (self.name, self.simulation_yml)

    def __str__(self):
        return str((self.path, self.name))


if __name__ == "__main__":
    import sys
    s = SesameProject(sys.argv[1])
    print s
    print repr(s)
    print s.parse_simulation_yml()
    print "cleaning yml"
    s.clean_yml()
    print "updating yml"
    s.update_yml()
    print "running app/arch sim"
    print s.runapparch()
    print "making app trace"
    print s.trace()
    print "running arch (from app trace)"
    print s.runarch()
